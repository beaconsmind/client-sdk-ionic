#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

// Define the plugin using the CAP_PLUGIN Macro, and
// each method the plugin supports using the CAP_PLUGIN_METHOD macro.
CAP_PLUGIN(BeaconsmindPlugin, "Beaconsmind",
           CAP_PLUGIN_METHOD(initialize, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(initializeDevelop, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(signUp, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(importAccount, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(login, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(logout, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(getOAuthContext, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(updateHostname, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(registerDeviceToken, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(getProfile, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(updateProfile, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(loadOffers, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(loadOffer, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(markOfferAsRead, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(markOfferAsReceived, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(markOfferAsRedeemed, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(startListeningBeacons, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(stopListeningBeacons, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(getBeaconContactsSummary, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(setMinLogLevel, CAPPluginReturnPromise);
)
