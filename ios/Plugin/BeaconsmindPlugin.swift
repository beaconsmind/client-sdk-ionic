import Foundation
import Capacitor
import Beaconsmind
import CoreLocation

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(BeaconsmindPlugin)
public class BeaconsmindPlugin: CAPPlugin, BeaconsmindDelegate, BeaconListenerDelegate {
    @objc func initialize(_ call: CAPPluginCall) {
        guard let hostname = call.getString("hostname") else {
            call.reject("hostname is required")
            return
        }

        let appVersion = call.getString("appVersion") ?? "unknown"

        do {
            try
                Beaconsmind.default.start(
                    delegate: self,
                    appVersion: appVersion,
                    hostname: hostname
                )
            call.resolve([
                "result": true
            ])
        } catch {
            call.reject("initialize failed", nil, error)
        }
    }

    @objc func initializeDevelop(_ call: CAPPluginCall) {
        guard let hostname = call.getString("hostname") else {
            call.reject("hostname is required")
            return
        }

        let appVersion = call.getString("appVersion") ?? "unknown"

        do {
            try
                Beaconsmind.default.startDevelop(
                    delegate: self,
                    appVersion: appVersion,
                    hostname: hostname
                )
            call.resolve([
                "result": true
            ])
        } catch {
            call.reject("initializeDevelop failed", nil, error)
        }
    }

    @objc func signUp(_ call: CAPPluginCall) {
        do {
            guard let username = call.getString("username") else {
                call.reject("username is required")
                return
            }
            guard let firstName = call.getString("firstName") else {
                call.reject("firstName is required")
                return
            }
            guard let lastName = call.getString("lastName") else {
                call.reject("lastName is required")
                return
            }
            guard let password = call.getString("password") else {
                call.reject("password is required")
                return
            }
            guard let confirmPassword = call.getString("confirmPassword") else {
                call.reject("confirmPassword is required")
                return
            }
            let language = call.getString("language")
            let gender = call.getString("gender")
            let favoriteStoreID = call.getInt("favoriteStoreID")
            let birthDateSeconds = call.getDouble("birthDateSeconds")
            let countryCode = call.getString("countryCode")

            try Beaconsmind.default.signup(
                username: username,
                firstName: firstName,
                lastName: lastName,
                password: password,
                confirmPassword: confirmPassword,
                language: language,
                gender: gender,
                favoriteStoreID: favoriteStoreID,
                birthDate: getDateDayFromSeconds(seconds: birthDateSeconds),
                countryCode: countryCode
            ) { resp in
                switch resp {
                case let .success(ctx):
                    call.resolve([
                        "userId": ctx.userID
                    ])
                case let .failure(error):
                    call.reject("signup failed", nil, error)
                }
            }
        } catch {
            call.reject("signup failed", nil, error)
        }
    }

    @objc func importAccount(_ call: CAPPluginCall) {
        guard let id = call.getString("id") else {
            call.reject("id is required")
            return
        }
        guard let email = call.getString("email") else {
            call.reject("email is required")
            return
        }
        let firstName = call.getString("firstName")
        let lastName = call.getString("lastName")
        let language = call.getString("language")
        let gender = call.getString("gender")
        let birthDateSeconds = call.getDouble("birthDateSeconds")

        do {
            try
                Beaconsmind.default.importAccount(
                    id: id,
                    email: email,
                    firstName: firstName,
                    lastName: lastName,
                    birthDate: getDateDayFromSeconds(seconds: birthDateSeconds)?.date,
                    language: language,
                    gender: gender) { resp in
                    switch resp {
                    case let .success(ctx):
                        call.resolve([
                            "userId": ctx.userID
                        ])
                    case let .failure(error):
                        call.reject("importAccount failed", nil, error)
                    }
                }
        } catch {
            call.reject("importAccount failed", nil, error)
        }
    }

    @objc func login(_ call: CAPPluginCall) {
        guard let username = call.getString("username") else {
            call.reject("username is required")
            return
        }
        guard let password = call.getString("password") else {
            call.reject("password is required")
            return
        }

        do {
            try
                Beaconsmind.default.login(
                    username: username,
                    password: password) { resp in
                    switch resp {
                    case let .success(ctx):
                        call.resolve([
                            "userId": ctx.userID
                        ])
                    case let .failure(error):
                        call.reject("login failed", nil, error)
                    }
                }
        } catch {
            do {
                try Beaconsmind.default.logout()
                call.resolve([
                    "result": true
                ])
            } catch {
                call.reject("logout failed", nil, error)
            }
        }
    }

    @objc func logout(_ call: CAPPluginCall) {
        do {
            try Beaconsmind.default.logout()
            call.resolve([
                "result": true
            ])
        } catch {
            call.reject("logout failed", nil, error)
        }
    }

    @objc func getOAuthContext(_ call: CAPPluginCall) {
        let ctx = Beaconsmind.default.getAPIContext()
        if ctx != nil {
            call.resolve([
                "userId": ctx!.userID
            ])
        } else {
            call.resolve([:])
        }
    }

    @objc func updateHostname(_ call: CAPPluginCall) {
        guard let hostname = call.getString("hostname") else {
            call.reject("hostname is required")
            return
        }

        Beaconsmind.default.updateHostname(hostname: hostname)
        call.resolve([
            "result": true
        ])
    }

    @objc func registerDeviceToken(_ call: CAPPluginCall) {
        guard let deviceToken = call.getString("deviceToken") else {
            call.reject("deviceToken is required")
            return
        }

        do {
            try Beaconsmind.default.register(deviceToken: deviceToken) { resp in
                switch resp {
                case .success:
                    call.resolve([
                        "result": true
                    ])
                case let .failure(error):
                    call.reject("registerDeviceToken failed", nil, error)
                }
            }
        } catch {
            call.reject("registerDeviceToken failed", nil, error)
        }
    }

    @objc func getProfile(_ call: CAPPluginCall) {
        do {
            try
                _ = Beaconsmind.default.getProfile { resp in
                    switch resp {
                    case let .success(profile):
                        let encoder = JSONEncoder()
                        encoder.dateEncodingStrategy = .iso8601
                        let data = try! encoder.encode(profile)
                        let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                        // TODO: date fields are returned like this: -393724800
                        call.resolve(dict)
                    case let .failure(error):
                        call.reject("getProfile failed!", nil, error)
                    }
                }
        } catch {
            call.reject("getProfile failed", nil, error)
        }
    }

    @objc func updateProfile(_ call: CAPPluginCall) {
        do {
            guard let firstName = call.getString("firstName") else {
                call.reject("firstName is required")
                return
            }
            guard let lastName = call.getString("lastName") else {
                call.reject("lastName is required")
                return
            }
            let birthDateSeconds = call.getDouble("birthDateSeconds")
            let city = call.getString("city")
            let country = call.getString("country")
            let disablePushNotifications = call.getBool("disablePushNotifications")
            let favoriteStoreID = call.getInt("favoriteStoreID")
            let gender = call.getString("gender")
            let houseNumber = call.getString("houseNumber")
            let landlinePhone = call.getString("landlinePhone")
            let language = call.getString("language")
            let phoneNumber = call.getString("phoneNumber")
            let street = call.getString("street")
            let zipCode = call.getString("zipCode")

            try
                _ = Beaconsmind.default.updateProfile(
                    firstName: firstName,
                    lastName: lastName,
                    birthDate: getDateDayFromSeconds(seconds: birthDateSeconds)?.date,
                    city: city,
                    country: country,
                    disablePushNotifications: disablePushNotifications,
                    favoriteStoreID: favoriteStoreID,
                    gender: gender,
                    houseNumber: houseNumber,
                    landlinePhone: landlinePhone,
                    language: language,
                    phoneNumber: phoneNumber,
                    street: street,
                    zipCode: zipCode) { resp in
                    switch resp {
                    case let .success(profile):
                        let encoder = JSONEncoder()
                        let data = try! encoder.encode(profile)
                        let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                        // TODO: date fields are returned like this: -393724800
                        call.resolve(dict)
                        break
                    case let .failure(error):
                        call.reject("updateProfile failed", nil, error)
                    }
                }
        } catch {
            call.reject("updateProfile failed", nil, error)
        }
    }

    @objc func loadOffers(_ call: CAPPluginCall) {
        do {
            let request = API.Offers.OffersGetOffers.Request()
            _ = try Beaconsmind.default.apiRequest(request) { res in
                switch res {
                case let .success(offers):
                    let data = try! JSONEncoder().encode(offers.success)
                    let list = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Dictionary<String, Any>]
                    call.resolve([
                        "offers": list
                    ])
                case let .failure(error):
                    call.reject("loadOffers failed", nil, error)
                }
            }
        } catch {
            call.reject("loadOffers failed", nil, error)
        }
    }

    @objc func loadOffer(_ call: CAPPluginCall) {
        guard let offerId = call.getInt("offerId") else {
            call.reject("offerId is required")
            return
        }
        do {
            let request = API.Offers.OffersGetOffer.Request(offerId: offerId)
            _ = try Beaconsmind.default.apiRequest(request) { res in
                switch res {
                case let .success(response):
                    let data = try! JSONEncoder().encode(response.success)
                    let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                    call.resolve(dict)
                case let .failure(error):
                    call.reject("loadOffers failed", nil, error)
                }
            }
        } catch {
            call.reject("loadOffer failed", nil, error)
        }
    }

    @objc func markOfferAsRead(_ call: CAPPluginCall) {
        guard let offerId = call.getInt("offerId") else {
            call.reject("offerId is required")
            return
        }
        do {
            try Beaconsmind.default.markOfferAsRead(offerID: offerId) { resp in
                switch resp {
                case .success:
                    call.resolve([
                        "result": true
                    ])
                case let .failure(error):
                    call.reject("markOfferAsRead failed", nil, error)
                }
            }
        } catch {
            call.reject("markOfferAsRead failed", nil, error)
        }
    }

    @objc func markOfferAsReceived(_ call: CAPPluginCall) {
        guard let offerId = call.getInt("offerId") else {
            call.reject("offerId is required")
            return
        }
        do {
            try Beaconsmind.default.markOfferAsRead(offerID: offerId) { resp in
                switch resp {
                case .success:
                    call.resolve([
                        "result": true
                    ])
                case let .failure(error):
                    call.reject("markOfferAsReceived failed", nil, error)
                }
            }
        } catch {
            call.reject("markOfferAsReceived failed", nil, error)
        }
    }

    @objc func markOfferAsRedeemed(_ call: CAPPluginCall) {
        guard let offerId = call.getInt("offerId") else {
            call.reject("offerId is required")
            return
        }
        do {
            try Beaconsmind.default.markOfferAsRedeemed(offerID: offerId) { resp in
                switch resp {
                case .success:
                    call.resolve([
                        "result": true
                    ])
                case let .failure(error):
                    call.reject("markOfferAsRedeemed failed", nil, error)
                }
            }
        } catch {
            call.reject("markOfferAsRedeemed failed", nil, error)
        }
    }

    @objc func startListeningBeacons(_ call: CAPPluginCall) {
        onMainThread { [unowned self] in
            do {
                try Beaconsmind.default.startListeningBeacons(delegate: self)
                call.resolve([
                    "result": true
                ])
            } catch {
                call.reject("startListeningBeacons failed", nil, error)
            }
        }
    }

    @objc func stopListeningBeacons(_ call: CAPPluginCall) {
        Beaconsmind.default.stopListeningBeacons()
        call.resolve([
            "result": true
        ])
    }

    @objc func getBeaconContactsSummary(_ call: CAPPluginCall) {
        do {
            let beaconContactsSummary = Beaconsmind.default.beaconContactsSummaries
            let list = beaconContactsSummary.map { summary -> NSDictionary in
                return [
                    "uuid": summary.uuid,
                    "major": summary.major,
                    "minor": summary.minor,
                    "name": summary.name,
                    "store": summary.store,
                    "currentRSSI": summary.rssi,
                    "averageRSSI": summary.averageRSSI,
                    "lastContactTimestamp": summary.timestamp,
                    "contactFrequencyPerMinute": summary.frequency,
                    "contactsCount": summary.contactCount,
                    "isInRange": summary.isInRange
                ]
            }
            call.resolve([
                "beacons": list
            ])
        } catch {
            call.reject("getBeaconsSummary failed", nil, error)
        }
    }

    @objc func setMinLogLevel(_ call: CAPPluginCall) {
        guard let minLogLevelString = call.getString("minLogLevel") else {
            call.reject("minLogLevel is required")
            return
        }

        let minLogLevel = LogLevel.init(rawValue: minLogLevelString) ?? LogLevel.silent
        Beaconsmind.default.setMinLogLevel(level: minLogLevel)

        call.resolve([
            "result": true
        ])
    }

    private func onMainThread(_ closure: @escaping () -> Void) {
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async {
                closure()
            }
        }
    }

    private func getDateDayFromSeconds(seconds: Double?) -> DateDay? {
        var d: DateDay?
        if seconds != nil {
            d = DateDay(date: Date(timeIntervalSince1970: seconds! ))
        }
        return d
    }

    public func ranged(beacons: [CLBeacon]) {}

    public func proximity(inRegion: CLBeaconRegion) {}

    public func proximity(leftRegion: CLBeaconRegion) {}

    public func ranged(region: CLRegion?, error: Error) {}

    public func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?) {}
}
