# @beaconsmind/ionic-sdk

[Beaconsmind](https://beaconsmind.com/) SDK for Ionic.

## Prerequisites

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by
Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `https://adidas.bms.beaconsmind.com`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

## 1. Installing

```bash
npm install @beaconsmind/ionic-sdk
npx cap sync
```

Make sure your _iOS Deployment Target_ is 11.0 or above.

Make sure your Android _minSdkVersion_ is 23 or above.

## 2. Permissions

Beaconsmind SDK requires bluetooth and location permission (whenInUse or always) to detect nearby beacon devices and notification permission to send offers. Consider using a package of choice to request those permissions. Make sure to follow the setup required for native iOS & Android.

## 3. Initialize the SDK

This will initialize the SDK and try to retrieve the logged-in user. Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.

```ts
import { Beaconsmind } from '@beaconsmind/ionic-sdk';

const initializeResponse = await Beaconsmind.initialize({
  hostname: 'https://test-develop-suite.azurewebsites.net/',
  appVersion: '0.0.1',
  platformOptions: {
    android: {
      usePassiveScanning: true,
      notification: {
        androidNotificationBadgeName: 'ic_beacons',
        androidNotificationChannelName: 'beaconsmind',
        androidNotificationTitle: 'Beaconsmind sdk demo',
        androidNotificationText: 'Listening to beacons',
      },
    },
  },
});

console.log(initializeResponse);
```

See:
* [`initialize(...)`](#initialize)

## 4. Authentication

### When using Beaconsmind authentication

This approach implies that the customer data is kept in Beaconsmind backend. Use UserManager.login method to perform
customer authentication.

To login existing user.

```ts
const loginResponse = await Beaconsmind.login({
  username: usernameValue,
  password: passwordValue,
});

console.log(loginResponse);
```

See:
* [`login(...)`](#login)

To register a new user.

```ts
const signUpResponse = await Beaconsmind.instance.signup({
    username: usernameValue,
    firstName: firstNameValue,
    lastName: lastnameValue,
    password: passwordValue,
    confirmPassword: confirmPasswordValue,
  });

console.log(signUpResponse);
```

See:
* [`signUp(...)`](#signup)

### When using own authentication mechanism

This approach implies that the customer data is kept in clients backend. The authentication is done by the client app
before the Beaconsmind API is used. Whenever customer is authenticated, use `Beaconsmind.importAccount` method
to send basic customer data to Beaconsmind. The customer data is updated only the first time the method is called. The
client app can track if the customer was imported before and omit personal data from following importAccount calls.

```ts
const importAccountResponse = await Beaconsmind.importAccount({
  id: '3287e422-961a-4d47-8eaa-a58e0bd536a1',
  email: 'account@example.com',
  firstName: 'John',
  lastName: 'Doe',
  language: 'en',
  gender: 'male',
  // UNIX timestamp represented in seconds.
  birthDateSeconds: 831589200, 
});

console.log(importAccountResponse);
```

See:
* [`importAccount(...)`](#importaccount)

## Log out

In order to log out the current account and stop listening for beacons, call the `Beaconsmind.logout()` method.

See:
* [`logout()`](#logout)

## 6. Notifications

The SDK uses _APNS_ for iOS, and _Firebase_ for Android. You can use the [@capacitor/push-notifications](https://www.npmjs.com/package/@capacitor/push-notifications) package in order to configure it.

### Setting device token

```ts
import { Beaconsmind } from '@beaconsmind/ionic-sdk';
import { PushNotifications } from '@capacitor/push-notifications';

PushNotifications.addListener('registration', async token => {
  alert('Registration token: ' + token.value);

  const registerDeviceTokenResponse = await Beaconsmind.registerDeviceToken({
    deviceToken: token.value,
  });

  console.log(registerDeviceTokenResponse);
});
```

See:
* [`registerDeviceToken(...)`](#registerdevicetoken)

### Supporting multiple push notification providers

**Issue**

On Android, if there is more than one package used to receive push notifications, for example: [capacitor-moengage-core](https://www.npmjs.com/package/capacitor-moengage-core) & [@capacitor/push-notifications](https://www.npmjs.com/package/@capacitor/push-notifications), then only one package will be notified about a push notification.

**Explanation**

Each package used to receive push notifications has it’s on `AndroidManifest.xml` file (manifest) where it declares a service that should receive and handle the FCM messages — [here](https://github.com/ionic-team/capacitor-plugins/blob/2e883f39329626062bcf876fe7d424432617229d/push-notifications/android/src/main/AndroidManifest.xml#L3-L7) is an example from the @capacitor/push-notifications package.

When app is built, the app’s manifest is merged with each manifest from packages that it depends on. As a result, the final manifest will contain multiple services declared to receive FCM messages, but the system will deliver push notification only to one of them, the one that is declared earlier.

If there is a service declaration at the app’s manifest, then it will take a precedence over service declarations from packages’ manifest.

**Solution**

In order to deliver push notifications to services declared by each package, a proxy service declared at the app’s manifest needs to be introduced. It will be responsible for intercepting push notifications and forwarding them to other services coming from used packages.

**Implementation**

1. Add [FirebaseMessagingServiceProxy.kt](example/android/app/src/main/java/com/beaconsmind/ioniccapacitordemo/FirebaseMessagingServiceProxy.kt) file to your Android app project.
2. In the `FirebaseMessagingServiceProxy` file, update `messagingServices` list with push notification services that you want to notify. Make sure to add necessary imports at the top of the file in order to access them.
3. Register the `FirebaseMessgingServiceProxy` in the [app’s manifest](example/android/app/src/main/AndroidManifest.xml).

## 7. Offers

Beacons ranging is used to pick up offers. In respect to offers, when users receive pushes about a new offer, read an offer and consume an offer, API calls need to be sent to recognize user activity.

### Getting offers

* [`loadOffers()`](#loadoffers)
* [`loadOffer(...)`](#loadoffer)

### Interacting with offers

* [`markOfferAsReceived(...)`](#markofferasreceived)
* [`markOfferAsRead(...)`](#markofferasread)
* [`markOfferAsRedeemed(...)`](#markofferasredeemed)

## 8. Beacons

### Interacting with beacons

Beacons feature uses BLE transmitters which can be picked up by devices. In Beaconsmind, BLE transmitters are used to present offers based on a physical location of a device.

Every beacon is defined by it's UUID and major and minor numbers. A list of beacons is persisted and maintained on Beaconsmind environment for each client.

In order to start monitoring nearby beacon devices, call `Beaconsmind.startListeningBeacons()`. Make sure that the bluetooth & location permissions have been granted. To stop the monitoring process, call `Beaconsmind.stopListeningBeacons()`. In order to obtain a list of beacon devices that are within range, call `Beaconsmind.getBeaconContactsSummary()`.

See:
* [`startListeningBeacons()`](#startlisteningbeacons)
* [`stopListeningBeacons()`](#stoplisteningbeacons)
* [`getBeaconContactsSummary()`](#getbeaconcontactssummary)

### Read more about beacons

- [iOS setup](https://gitlab.com/beaconsmind/client-sdk-ios/-/blob/main/Documentation/Beacons.md).
- [Android setup](https://gitlab.com/beaconsmind/client-sdk-android/-/blob/main/Documentation/Beacons.md).

## Deployment

Log in into the npm package registry by running

```bash
npm login
```

In the root directory, run:

```bash
npx release-it
```

For the wizard questions, answer in the following way: 

- Publish @beaconsmind/ionic-sdk to npm? Yes
- Commit? Yes
- Tag (x.y.z)? Yes
- Push? Yes

## API

<docgen-index>

* [`setMinLogLevel(...)`](#setminloglevel)
* [`initialize(...)`](#initialize)
* [`initializeDevelop(...)`](#initializedevelop)
* [`login(...)`](#login)
* [`signUp(...)`](#signup)
* [`importAccount(...)`](#importaccount)
* [`logout()`](#logout)
* [`getOAuthContext()`](#getoauthcontext)
* [`getProfile()`](#getprofile)
* [`updateProfile(...)`](#updateprofile)
* [`registerDeviceToken(...)`](#registerdevicetoken)
* [`loadOffers()`](#loadoffers)
* [`loadOffer(...)`](#loadoffer)
* [`markOfferAsReceived(...)`](#markofferasreceived)
* [`markOfferAsRead(...)`](#markofferasread)
* [`markOfferAsRedeemed(...)`](#markofferasredeemed)
* [`startListeningBeacons()`](#startlisteningbeacons)
* [`stopListeningBeacons()`](#stoplisteningbeacons)
* [`updateHostname(...)`](#updatehostname)
* [`getBeaconContactsSummary()`](#getbeaconcontactssummary)
* [Interfaces](#interfaces)
* [Type Aliases](#type-aliases)
* [Enums](#enums)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### setMinLogLevel(...)

```typescript
setMinLogLevel({ minLogLevel }: { minLogLevel: LogLevel; }) => Promise<Response>
```

Sets the minimum log level.

| Param     | Type                                                            |
| --------- | --------------------------------------------------------------- |
| **`__0`** | <code>{ minLogLevel: <a href="#loglevel">LogLevel</a>; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### initialize(...)

```typescript
initialize(options: InitializeOptions) => Promise<Response>
```

Initializes the SDK.

Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.

| Param         | Type                                                            |
| ------------- | --------------------------------------------------------------- |
| **`options`** | <code><a href="#initializeoptions">InitializeOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### initializeDevelop(...)

```typescript
initializeDevelop(options: InitializeOptions) => Promise<Response>
```

**For development purposes only.** Alternative initialization method.

Kickstart sdk testing and integration by a single method call. Invokes [initialize] and sets the sdk in development mode.

When initialized with this method the sdk requests permissions on it own by.

* Your app **must** use activity which implements [ActivityResultCaller].

| Param         | Type                                                            |
| ------------- | --------------------------------------------------------------- |
| **`options`** | <code><a href="#initializeoptions">InitializeOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### login(...)

```typescript
login({ username, password }: { username: string; password: string; }) => Promise<Response>
```

Performs login and immediately starts listening for beacons

| Param     | Type                                                 |
| --------- | ---------------------------------------------------- |
| **`__0`** | <code>{ username: string; password: string; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### signUp(...)

```typescript
signUp(options: SignUpOptions) => Promise<UserIdResponse>
```

Creates a new account login and immediately starts listening for beacons.

| Param         | Type                                                    |
| ------------- | ------------------------------------------------------- |
| **`options`** | <code><a href="#signupoptions">SignUpOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#useridresponse">UserIdResponse</a>&gt;</code>

--------------------


### importAccount(...)

```typescript
importAccount(options: ImportAccountOptions) => Promise<UserIdResponse>
```

When the customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer's private data,
this endpoint is used to import the customer into Beaconsmind and to obtain a token with which the app can send tracking data to us.
The token does NOT provide access to any personal customer info.
If personal data is present in the request, it will only be updated the first time the endpoint is invoked for that customer.
Once the authentication is done by the app, the username is sent to Beaconsmind using /api/accounts/import to receive a JWT for access to Beaconsmind backend.
In this case, Beaconsmind will not allow any personal data to be accessed and it’s the apps responsibility to only send personal data the first time /api/accounts/import is used.
When using import, editing profile is disabled.

| Param         | Type                                                                  |
| ------------- | --------------------------------------------------------------------- |
| **`options`** | <code><a href="#importaccountoptions">ImportAccountOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#useridresponse">UserIdResponse</a>&gt;</code>

--------------------


### logout()

```typescript
logout() => Promise<Response>
```

Logs out the current account and stops listening for beacons.

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### getOAuthContext()

```typescript
getOAuthContext() => Promise<UserIdResponse>
```

Returns the currently logged in user id.

**Returns:** <code>Promise&lt;<a href="#useridresponse">UserIdResponse</a>&gt;</code>

--------------------


### getProfile()

```typescript
getProfile() => Promise<ProfileResponse>
```

Returns the currently logged in user profile data.

**Returns:** <code>Promise&lt;<a href="#profileresponse">ProfileResponse</a>&gt;</code>

--------------------


### updateProfile(...)

```typescript
updateProfile(options: any) => Promise<ProfileResponse>
```

Updates the currently logged in user profile data.

| Param         | Type             |
| ------------- | ---------------- |
| **`options`** | <code>any</code> |

**Returns:** <code>Promise&lt;<a href="#profileresponse">ProfileResponse</a>&gt;</code>

--------------------


### registerDeviceToken(...)

```typescript
registerDeviceToken({ deviceToken }: { deviceToken: string; }) => Promise<Response>
```

Register the device token in Beaconsmind servers in order receive offers push notifications.

| Param     | Type                                  |
| --------- | ------------------------------------- |
| **`__0`** | <code>{ deviceToken: string; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### loadOffers()

```typescript
loadOffers() => Promise<OffersResponse>
```

Loads the list of offers for the current user.

**Returns:** <code>Promise&lt;<a href="#offersresponse">OffersResponse</a>&gt;</code>

--------------------


### loadOffer(...)

```typescript
loadOffer({ offerId }: { offerId: number; }) => Promise<Offer>
```

Loads the offer details for the given offer id.

| Param     | Type                              |
| --------- | --------------------------------- |
| **`__0`** | <code>{ offerId: number; }</code> |

**Returns:** <code>Promise&lt;<a href="#offer">Offer</a>&gt;</code>

--------------------


### markOfferAsReceived(...)

```typescript
markOfferAsReceived({ offerId }: { offerId: number; }) => Promise<Response>
```

Marks the offer as received.

Call this when an offer is received via push notification.

| Param     | Type                              |
| --------- | --------------------------------- |
| **`__0`** | <code>{ offerId: number; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### markOfferAsRead(...)

```typescript
markOfferAsRead({ offerId }: { offerId: number; }) => Promise<Response>
```

Marks the offer as read.

Call this when the user opens the offer.

| Param     | Type                              |
| --------- | --------------------------------- |
| **`__0`** | <code>{ offerId: number; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### markOfferAsRedeemed(...)

```typescript
markOfferAsRedeemed({ offerId }: { offerId: number; }) => Promise<Response>
```

Marks the offer as redeemed.

Call this when the user redeems the offer.

| Param     | Type                              |
| --------- | --------------------------------- |
| **`__0`** | <code>{ offerId: number; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### startListeningBeacons()

```typescript
startListeningBeacons() => Promise<Response>
```

Starts listening for beacons devices. It requires the following permissions to work:
- location (whenInUse/always)
- bluetooth

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### stopListeningBeacons()

```typescript
stopListeningBeacons() => Promise<Response>
```

Stops listening for beacons devices.

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### updateHostname(...)

```typescript
updateHostname({ hostname }: { hostname: string; }) => Promise<Response>
```

Updates the hostname that the SDK will use to connect to the Beaconsmind servers.

| Param     | Type                               |
| --------- | ---------------------------------- |
| **`__0`** | <code>{ hostname: string; }</code> |

**Returns:** <code>Promise&lt;<a href="#response">Response</a>&gt;</code>

--------------------


### getBeaconContactsSummary()

```typescript
getBeaconContactsSummary() => Promise<BeaconContactSummaryResponse>
```

Get a list of all the beacons that are currently in range.

**Returns:** <code>Promise&lt;<a href="#beaconcontactsummaryresponse">BeaconContactSummaryResponse</a>&gt;</code>

--------------------


### Interfaces


#### InitializeOptions

| Prop                  | Type                                                                            |
| --------------------- | ------------------------------------------------------------------------------- |
| **`appVersion`**      | <code>string</code>                                                             |
| **`hostname`**        | <code>string</code>                                                             |
| **`platformOptions`** | <code><a href="#initializeplatformoptions">InitializePlatformOptions</a></code> |


#### InitializePlatformOptions

| Prop          | Type                                                                          |
| ------------- | ----------------------------------------------------------------------------- |
| **`android`** | <code><a href="#initializeandroidoptions">InitializeAndroidOptions</a></code> |


#### InitializeAndroidOptions

| Prop                     | Type                                                                                      | Description                                                                                                                                                                                                                                                                                                            |
| ------------------------ | ----------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **`notification`**       | <code><a href="#androidnotificationoptions">AndroidNotificationOptions</a> \| null</code> | Options that describe the content of the native Android notification that is displayed to user when a foreground service responsible for scanning. The notification is displayed when [usePassiveScanning] is set to `false`.                                                                                          |
| **`usePassiveScanning`** | <code>boolean</code>                                                                      | The SDK supports two types of BLE scanning: - active scanning, - passive scanning. When scanning actively, the Beaconsmind SDK will start a foreground service and show a pinned notification described by [notificationOptions]. By default, the SDK uses passive scanning, i.e. [usePassiveScanning] is set to true. |


#### AndroidNotificationOptions

| Prop                                 | Type                | Description                                                                                                                                                                                                                   |
| ------------------------------------ | ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **`androidNotificationBadgeName`**   | <code>string</code> | Name of the native icon resource. Default value: "ic_beacons". Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setSmallIcon(int)                                              |
| **`androidNotificationTitle`**       | <code>string</code> | Title of the notification (first row). Default value: "Beaconsmind". Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentTitle(java.lang.CharSequence)                  |
| **`androidNotificationText`**        | <code>string</code> | Body of the notification (second row). Default value: "Listening for Beacons". Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentText(java.lang.CharSequence)         |
| **`androidNotificationChannelName`** | <code>string</code> | The user visible name of the channel. Default value: "beaconsmind". Reference: https://developer.android.com/reference/android/app/NotificationChannel#NotificationChannel(java.lang.String,%20java.lang.CharSequence,%20int) |


#### SignUpOptions

| Prop                   | Type                |
| ---------------------- | ------------------- |
| **`username`**         | <code>string</code> |
| **`firstName`**        | <code>string</code> |
| **`lastName`**         | <code>string</code> |
| **`password`**         | <code>string</code> |
| **`confirmPassword`**  | <code>string</code> |
| **`language`**         | <code>string</code> |
| **`gender`**           | <code>string</code> |
| **`favoriteStoreID`**  | <code>number</code> |
| **`birthDateSeconds`** | <code>number</code> |
| **`countryCode`**      | <code>string</code> |


#### ImportAccountOptions

| Prop                   | Type                |
| ---------------------- | ------------------- |
| **`id`**               | <code>string</code> |
| **`email`**            | <code>string</code> |
| **`firstName`**        | <code>string</code> |
| **`lastName`**         | <code>string</code> |
| **`language`**         | <code>string</code> |
| **`gender`**           | <code>string</code> |
| **`birthDateSeconds`** | <code>number</code> |


#### ProfileResponse

| Prop                           | Type                                          |
| ------------------------------ | --------------------------------------------- |
| **`id`**                       | <code>string</code>                           |
| **`userName`**                 | <code>string</code>                           |
| **`claims`**                   | <code>string[]</code>                         |
| **`roles`**                    | <code>string[]</code>                         |
| **`gender`**                   | <code>string \| null</code>                   |
| **`firstName`**                | <code>string</code>                           |
| **`lastName`**                 | <code>string</code>                           |
| **`fullName`**                 | <code>string</code>                           |
| **`street`**                   | <code>string \| null</code>                   |
| **`houseNumber`**              | <code>string \| null</code>                   |
| **`zipCode`**                  | <code>string \| null</code>                   |
| **`city`**                     | <code>string \| null</code>                   |
| **`country`**                  | <code>string \| null</code>                   |
| **`landlinePhone`**            | <code>string \| null</code>                   |
| **`phoneNumber`**              | <code>string \| null</code>                   |
| **`url`**                      | <code>string \| null</code>                   |
| **`disablePushNotifications`** | <code>boolean</code>                          |
| **`newsLetterSubscription`**   | <code>boolean</code>                          |
| **`joinDate`**                 | <code><a href="#date">Date</a></code>         |
| **`birthDate`**                | <code><a href="#date">Date</a> \| null</code> |
| **`clubId`**                   | <code>string \| null</code>                   |
| **`favoriteStore`**            | <code>string \| null</code>                   |
| **`favoriteStoreId`**          | <code>number \| null</code>                   |


#### Date

Enables basic storage and retrieval of dates and times.

| Method                 | Signature                                                                                                    | Description                                                                                                                             |
| ---------------------- | ------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------- |
| **toString**           | () =&gt; string                                                                                              | Returns a string representation of a date. The format of the string depends on the locale.                                              |
| **toDateString**       | () =&gt; string                                                                                              | Returns a date as a string value.                                                                                                       |
| **toTimeString**       | () =&gt; string                                                                                              | Returns a time as a string value.                                                                                                       |
| **toLocaleString**     | () =&gt; string                                                                                              | Returns a value as a string value appropriate to the host environment's current locale.                                                 |
| **toLocaleDateString** | () =&gt; string                                                                                              | Returns a date as a string value appropriate to the host environment's current locale.                                                  |
| **toLocaleTimeString** | () =&gt; string                                                                                              | Returns a time as a string value appropriate to the host environment's current locale.                                                  |
| **valueOf**            | () =&gt; number                                                                                              | Returns the stored time value in milliseconds since midnight, January 1, 1970 UTC.                                                      |
| **getTime**            | () =&gt; number                                                                                              | Gets the time value in milliseconds.                                                                                                    |
| **getFullYear**        | () =&gt; number                                                                                              | Gets the year, using local time.                                                                                                        |
| **getUTCFullYear**     | () =&gt; number                                                                                              | Gets the year using Universal Coordinated Time (UTC).                                                                                   |
| **getMonth**           | () =&gt; number                                                                                              | Gets the month, using local time.                                                                                                       |
| **getUTCMonth**        | () =&gt; number                                                                                              | Gets the month of a <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                             |
| **getDate**            | () =&gt; number                                                                                              | Gets the day-of-the-month, using local time.                                                                                            |
| **getUTCDate**         | () =&gt; number                                                                                              | Gets the day-of-the-month, using Universal Coordinated Time (UTC).                                                                      |
| **getDay**             | () =&gt; number                                                                                              | Gets the day of the week, using local time.                                                                                             |
| **getUTCDay**          | () =&gt; number                                                                                              | Gets the day of the week using Universal Coordinated Time (UTC).                                                                        |
| **getHours**           | () =&gt; number                                                                                              | Gets the hours in a date, using local time.                                                                                             |
| **getUTCHours**        | () =&gt; number                                                                                              | Gets the hours value in a <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                       |
| **getMinutes**         | () =&gt; number                                                                                              | Gets the minutes of a <a href="#date">Date</a> object, using local time.                                                                |
| **getUTCMinutes**      | () =&gt; number                                                                                              | Gets the minutes of a <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                           |
| **getSeconds**         | () =&gt; number                                                                                              | Gets the seconds of a <a href="#date">Date</a> object, using local time.                                                                |
| **getUTCSeconds**      | () =&gt; number                                                                                              | Gets the seconds of a <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                           |
| **getMilliseconds**    | () =&gt; number                                                                                              | Gets the milliseconds of a <a href="#date">Date</a>, using local time.                                                                  |
| **getUTCMilliseconds** | () =&gt; number                                                                                              | Gets the milliseconds of a <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                      |
| **getTimezoneOffset**  | () =&gt; number                                                                                              | Gets the difference in minutes between the time on the local computer and Universal Coordinated Time (UTC).                             |
| **setTime**            | (time: number) =&gt; number                                                                                  | Sets the date and time value in the <a href="#date">Date</a> object.                                                                    |
| **setMilliseconds**    | (ms: number) =&gt; number                                                                                    | Sets the milliseconds value in the <a href="#date">Date</a> object using local time.                                                    |
| **setUTCMilliseconds** | (ms: number) =&gt; number                                                                                    | Sets the milliseconds value in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                              |
| **setSeconds**         | (sec: number, ms?: number \| undefined) =&gt; number                                                         | Sets the seconds value in the <a href="#date">Date</a> object using local time.                                                         |
| **setUTCSeconds**      | (sec: number, ms?: number \| undefined) =&gt; number                                                         | Sets the seconds value in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                   |
| **setMinutes**         | (min: number, sec?: number \| undefined, ms?: number \| undefined) =&gt; number                              | Sets the minutes value in the <a href="#date">Date</a> object using local time.                                                         |
| **setUTCMinutes**      | (min: number, sec?: number \| undefined, ms?: number \| undefined) =&gt; number                              | Sets the minutes value in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                   |
| **setHours**           | (hours: number, min?: number \| undefined, sec?: number \| undefined, ms?: number \| undefined) =&gt; number | Sets the hour value in the <a href="#date">Date</a> object using local time.                                                            |
| **setUTCHours**        | (hours: number, min?: number \| undefined, sec?: number \| undefined, ms?: number \| undefined) =&gt; number | Sets the hours value in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                     |
| **setDate**            | (date: number) =&gt; number                                                                                  | Sets the numeric day-of-the-month value of the <a href="#date">Date</a> object using local time.                                        |
| **setUTCDate**         | (date: number) =&gt; number                                                                                  | Sets the numeric day of the month in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                        |
| **setMonth**           | (month: number, date?: number \| undefined) =&gt; number                                                     | Sets the month value in the <a href="#date">Date</a> object using local time.                                                           |
| **setUTCMonth**        | (month: number, date?: number \| undefined) =&gt; number                                                     | Sets the month value in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                     |
| **setFullYear**        | (year: number, month?: number \| undefined, date?: number \| undefined) =&gt; number                         | Sets the year of the <a href="#date">Date</a> object using local time.                                                                  |
| **setUTCFullYear**     | (year: number, month?: number \| undefined, date?: number \| undefined) =&gt; number                         | Sets the year value in the <a href="#date">Date</a> object using Universal Coordinated Time (UTC).                                      |
| **toUTCString**        | () =&gt; string                                                                                              | Returns a date converted to a string using Universal Coordinated Time (UTC).                                                            |
| **toISOString**        | () =&gt; string                                                                                              | Returns a date as a string value in ISO format.                                                                                         |
| **toJSON**             | (key?: any) =&gt; string                                                                                     | Used by the JSON.stringify method to enable the transformation of an object's data for JavaScript Object Notation (JSON) serialization. |


#### OffersResponse

| Prop         | Type                 |
| ------------ | -------------------- |
| **`offers`** | <code>Offer[]</code> |


#### Offer

| Prop               | Type                                                  |
| ------------------ | ----------------------------------------------------- |
| **`offerId`**      | <code>number</code>                                   |
| **`title`**        | <code>string</code>                                   |
| **`text`**         | <code>string</code>                                   |
| **`imageUrl`**     | <code>string</code>                                   |
| **`thumbnailUrl`** | <code>string</code>                                   |
| **`validity`**     | <code>string</code>                                   |
| **`isRedeemed`**   | <code>boolean</code>                                  |
| **`isRedeemable`** | <code>boolean</code>                                  |
| **`isVoucher`**    | <code>boolean</code>                                  |
| **`tileAmount`**   | <code>number</code>                                   |
| **`callToAction`** | <code>{ title: string; link: string; } \| null</code> |


#### BeaconContactSummaryResponse

| Prop          | Type                                |
| ------------- | ----------------------------------- |
| **`beacons`** | <code>BeaconContactSummary[]</code> |


#### BeaconContactSummary

A class that holds information about the beacon device.

It consists of the beacon unique identifier ([uuid], [minor], [major]),
the beacon's name ([name]) and the name of the [store] that it is located in.

Additionally, it contains data related to its signal strength and the number
of times it was contacted.

| Prop                            | Type                        | Description                                                                                                                                                                                                                    |
| ------------------------------- | --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **`uuid`**                      | <code>string</code>         | Beacon's uuid.                                                                                                                                                                                                                 |
| **`major`**                     | <code>string</code>         | Beacon's major.                                                                                                                                                                                                                |
| **`minor`**                     | <code>string</code>         | Beacon's minor.                                                                                                                                                                                                                |
| **`name`**                      | <code>string</code>         | Beacon's name.                                                                                                                                                                                                                 |
| **`store`**                     | <code>string</code>         | The name of the store that the beacon is located in.                                                                                                                                                                           |
| **`currentRSSI`**               | <code>number \| null</code> | Beacon's current signal strength in dB. Null if the beacon has not been contacted yet. RSSI is an abbreviation from Received Signal Strength Indicator. See: https://en.wikipedia.org/wiki/Received_signal_strength_indication |
| **`averageRSSI`**               | <code>number \| null</code> | Beacon's average signal strength in dB (based on the last contacts). Null if the beacon has not been contacted yet.                                                                                                            |
| **`lastContactTimestamp`**      | <code>number \| null</code> | Last beacon contact timestamp represented as unix timestamp in milliseconds. Null when the beacon has not been contacted yet.                                                                                                  |
| **`contactFrequencyPerMinute`** | <code>number</code>         | Contacts frequency per minute.                                                                                                                                                                                                 |
| **`contactsCount`**             | <code>number</code>         | Indicates how many times the beacon was contacted.                                                                                                                                                                             |
| **`isInRange`**                 | <code>boolean</code>        | Returns if the beacon is within range.                                                                                                                                                                                         |


### Type Aliases


#### Response

<code>{ result: boolean }</code>


#### UserIdResponse

<code>{ userId: string } | null</code>


### Enums


#### LogLevel

| Members       | Value                  | Description                                                                                                            |
| ------------- | ---------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| **`debug`**   | <code>'debug'</code>   | Any developer-level debug info, repeating events, e.g.: - monitoring/location callbacks, - networking success results. |
| **`info`**    | <code>'info'</code>    | Bigger events/steps in the SDK lifecycle, e.g.: - starting/stopping services, - initializing SDK, - one-time events.   |
| **`warning`** | <code>'warning'</code> | Allowed, but not-optimal, inconsistent state, e.g.: - trying to start monitoring without permissions.                  |
| **`error`**   | <code>'error'</code>   | Not allowed state (any error).                                                                                         |
| **`silent`**  | <code>'silent'</code>  | No logging at all.                                                                                                     |

</docgen-api>
