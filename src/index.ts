import { registerPlugin } from '@capacitor/core';

import type { BeaconsmindPlugin } from './definitions';

const Beaconsmind = registerPlugin<BeaconsmindPlugin>('Beaconsmind', {
  web: () => import('./web').then((m) => new m.BeaconsmindWeb()),
});

export * from './definitions';
export { Beaconsmind };
