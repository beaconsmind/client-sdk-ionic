import { WebPlugin } from '@capacitor/core';

import type {
  BeaconContactSummaryResponse,
  BeaconsmindPlugin,
  ImportAccountOptions,
  InitializeOptions,
  LogLevel,
  Offer,
  OffersResponse,
  ProfileResponse,
  Response,
  SignUpOptions,
  UserIdResponse,
} from './definitions';

export class BeaconsmindWeb extends WebPlugin implements BeaconsmindPlugin {
  async setMinLogLevel({ minLogLevel }: { minLogLevel: LogLevel }): Promise<Response> {
    console.log(minLogLevel);
    return { result: true };
  }

  async importAccount(options: ImportAccountOptions): Promise<UserIdResponse> {
    console.log(options);
    return { userId: '' };
  }

  async login({ username, password }: { username: string; password: string }): Promise<Response> {
    console.log(username);
    console.log(password);
    return { result: true };
  }

  async logout(): Promise<Response> {
    return { result: true };
  }

  async getOAuthContext(): Promise<UserIdResponse> {
    return { userId: '' };
  }

  async getProfile(): Promise<ProfileResponse> {
    return emptyProfile;
  }

  async updateProfile(options: any): Promise<ProfileResponse> {
    console.log(options);
    return emptyProfile;
  }

  async registerDeviceToken({ deviceToken }: { deviceToken: string }): Promise<Response> {
    console.log(deviceToken);
    return { result: true };
  }

  async markOfferAsRead({ offerId }: { offerId: number }): Promise<Response> {
    console.log(offerId);
    return { result: true };
  }

  async markOfferAsReceived({ offerId }: { offerId: number }): Promise<Response> {
    console.log(offerId);
    return { result: true };
  }

  async markOfferAsRedeemed({ offerId }: { offerId: number }): Promise<Response> {
    console.log(offerId);
    return { result: true };
  }

  async startListeningBeacons(): Promise<Response> {
    return { result: true };
  }

  async stopListeningBeacons(): Promise<Response> {
    return { result: true };
  }

  async updateHostname({ hostname }: { hostname: string }): Promise<Response> {
    console.log(hostname);
    return { result: true };
  }

  async getBeaconContactsSummary(): Promise<BeaconContactSummaryResponse> {
    return { beacons: [] };
  }

  async loadOffers(): Promise<OffersResponse> {
    return { offers: [] };
  }

  async loadOffer({ offerId }: { offerId: number }): Promise<Offer> {
    console.log(offerId);
    return emptyOffer;
  }

  async signUp(options: SignUpOptions): Promise<UserIdResponse> {
    console.log(options);
    return { userId: '' };
  }

  async initialize(options: InitializeOptions): Promise<Response> {
    console.log(options);
    return { result: true };
  }

  async initializeDevelop(options: InitializeOptions): Promise<Response> {
    console.log(options);
    return { result: true };
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    return options;
  }
}

const emptyProfile = {
  id: '',
  userName: '',
  claims: [],
  roles: [],
  gender: '',
  firstName: '',
  lastName: '',
  fullName: '',
  street: '',
  houseNumber: '',
  zipCode: '',
  city: '',
  country: '',
  landlinePhone: '',
  phoneNumber: '',
  url: '',
  disablePushNotifications: false,
  newsLetterSubscription: false,
  joinDate: new Date(),
  birthDate: new Date(),
  clubId: '',
  favoriteStore: '',
  favoriteStoreId: 0,
};

const emptyOffer = {
  offerId: 0,
  title: '',
  text: '',
  imageUrl: '',
  thumbnailUrl: '',
  validity: '',
  isRedeemed: false,
  isRedeemable: false,
  isVoucher: false,
  tileAmount: 0,
  callToAction: null,
};
