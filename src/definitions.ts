export interface BeaconsmindPlugin {
  /**
   * Sets the minimum log level.
   *
   * @param minLogLevel
   * @returns {Response} `true` if the log level was set, `false` otherwise.
   */
  setMinLogLevel({ minLogLevel }: { minLogLevel: LogLevel }): Promise<Response>;

  /**
   * Initializes the SDK.
   *
   * Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.
   *
   * @param options
   * @returns {Response} `true` if the SDK was initialized, `false` otherwise.
   */
  initialize(options: InitializeOptions): Promise<Response>;

  /**
   * **For development purposes only.** Alternative initialization method.
   *
   * Kickstart sdk testing and integration by a single method call. Invokes [initialize] and sets the sdk in development mode.
   *
   * When initialized with this method the sdk requests permissions on it own by.
   *
   * * Your app **must** use activity which implements [ActivityResultCaller].
   *
   * @param options
   * @returns {Response} `true` if the SDK was initialized, `false` otherwise.
   */
  initializeDevelop(options: InitializeOptions): Promise<Response>;

  /**
   * Performs login and immediately starts listening for beacons
   *
   * @param username
   * @param password
   * @returns {Response} `true` if the account was logged in, `false` otherwise.
   */
  login({ username, password }: { username: string; password: string }): Promise<Response>;

  /**
   * Creates a new account login and immediately starts listening for beacons.
   *
   * @param options
   * @returns {UserIdResponse} The currently logged in user id.
   */
  signUp(options: SignUpOptions): Promise<UserIdResponse>;

  /**
   * When the customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer's private data,
   * this endpoint is used to import the customer into Beaconsmind and to obtain a token with which the app can send tracking data to us.
   * The token does NOT provide access to any personal customer info.
   * If personal data is present in the request, it will only be updated the first time the endpoint is invoked for that customer.
   * Once the authentication is done by the app, the username is sent to Beaconsmind using /api/accounts/import to receive a JWT for access to Beaconsmind backend.
   * In this case, Beaconsmind will not allow any personal data to be accessed and it’s the apps responsibility to only send personal data the first time /api/accounts/import is used.
   * When using import, editing profile is disabled.
   *
   * @param options
   * @returns {Response} `true` if the account was imported, `false` otherwise.
   */
  importAccount(options: ImportAccountOptions): Promise<UserIdResponse>;

  /**
   * Logs out the current account and stops listening for beacons.
   *
   * @returns {Response} `true` if the account was logged out, `false` otherwise.
   */
  logout(): Promise<Response>;

  /**
   * Returns the currently logged in user id.
   *
   * @returns {UserIdResponse} The currently logged in user id.
   */
  getOAuthContext(): Promise<UserIdResponse>;

  /**
   * Returns the currently logged in user profile data.
   *
   * @returns {ProfileResponse} User profile data.
   */
  getProfile(): Promise<ProfileResponse>;

  /**
   * Updates the currently logged in user profile data.
   *
   * @param options
   * @returns {ProfileResponse} The updated user profile data.
   */
  updateProfile(options: any): Promise<ProfileResponse>;

  /**
   * Register the device token in Beaconsmind servers in order receive offers push notifications.
   *
   * @param deviceToken
   * @returns {Response} `true` if the device token was registered, `false` otherwise.
   */
  registerDeviceToken({ deviceToken }: { deviceToken: string }): Promise<Response>;

  /**
   * Loads the list of offers for the current user.
   *
   * @returns {OffersResponse} A list of offers.
   */
  loadOffers(): Promise<OffersResponse>;

  /**
   * Loads the offer details for the given offer id.
   *
   * @param offerId
   * @returns {Offer} The offer details.
   */
  loadOffer({ offerId }: { offerId: number }): Promise<Offer>;

  /**
   * Marks the offer as received.
   *
   * Call this when an offer is received via push notification.
   *
   * @param offerId
   * @returns {Response} `true` if the offer was marked as received, `false` otherwise.
   */
  markOfferAsReceived({ offerId }: { offerId: number }): Promise<Response>;

  /**
   * Marks the offer as read.
   *
   * Call this when the user opens the offer.
   *
   * @param offerId
   * @returns {Response} `true` if the offer was marked as read, `false` otherwise.
   */
  markOfferAsRead({ offerId }: { offerId: number }): Promise<Response>;

  /**
   * Marks the offer as redeemed.
   *
   * Call this when the user redeems the offer.
   *
   * @param offerId
   * @returns {Response} `true` if the offer was marked as redeemed, `false` otherwise.
   */
  markOfferAsRedeemed({ offerId }: { offerId: number }): Promise<Response>;

  /**
   * Starts listening for beacons devices. It requires the following permissions to work:
   * - location (whenInUse/always)
   * - bluetooth
   *
   * @returns {Response} `true` if the beacon listening was started, `false` otherwise.
   */
  startListeningBeacons(): Promise<Response>;

  /**
   * Stops listening for beacons devices.
   *
   * @returns {Response} `true` if the beacon listening was stopped, `false` otherwise.
   */
  stopListeningBeacons(): Promise<Response>;

  /**
   * Updates the hostname that the SDK will use to connect to the Beaconsmind servers.
   *
   * @param hostname
   * @returns {Response} `true` if the hostname was updated, `false` otherwise.
   */
  updateHostname({ hostname }: { hostname: string }): Promise<Response>;

  /**
   * Get a list of all the beacons that are currently in range.
   *
   * @returns {BeaconsResponse} A list of beacons.
   */
  getBeaconContactsSummary(): Promise<BeaconContactSummaryResponse>;
}

export type Response = { result: boolean };

export type UserIdResponse = { userId: string } | null;

export interface InitializeOptions {
  appVersion: string;
  hostname: string;
  platformOptions?: InitializePlatformOptions;
}

export interface InitializePlatformOptions {
  android?: InitializeAndroidOptions;
}

export interface InitializeAndroidOptions {
  /**
   * Options that describe the content of the native Android notification that
   * is displayed to user when a foreground service responsible for scanning.
   *
   * The notification is displayed when [usePassiveScanning] is set to `false`.
   */
  notification: AndroidNotificationOptions | null;

  /**
   * The SDK supports two types of BLE scanning:
   * - active scanning,
   * - passive scanning.
   *
   * When scanning actively, the Beaconsmind SDK will start a foreground service and show a pinned notification described by [notificationOptions].
   * By default, the SDK uses passive scanning, i.e. [usePassiveScanning] is set to true.
   */
  usePassiveScanning?: boolean;
}

export interface AndroidNotificationOptions {
  /**
   * Name of the native icon resource.
   *
   * Default value: "ic_beacons".
   *
   * Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setSmallIcon(int)
   */
  androidNotificationBadgeName: string;

  /**
   * Title of the notification (first row).
   *
   * Default value: "Beaconsmind".
   *
   * Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentTitle(java.lang.CharSequence)
   */
  androidNotificationTitle: string;

  /**
   * Body of the notification (second row).
   *
   * Default value: "Listening for Beacons".
   *
   * Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentText(java.lang.CharSequence)
   */
  androidNotificationText: string;

  /**
   * The user visible name of the channel.
   *
   * Default value: "beaconsmind".
   *
   * Reference: https://developer.android.com/reference/android/app/NotificationChannel#NotificationChannel(java.lang.String,%20java.lang.CharSequence,%20int)
   */
  androidNotificationChannelName: string;
}

export interface SignUpOptions {
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
  language?: string;
  gender?: string;
  favoriteStoreID?: number;
  birthDateSeconds?: number;
  countryCode?: string;
}

export enum LogLevel {
  /**
   * Any developer-level debug info, repeating events, e.g.:
   * - monitoring/location callbacks,
   * - networking success results.
   */
  debug = 'debug',

  /**
   * Bigger events/steps in the SDK lifecycle, e.g.:
   * - starting/stopping services,
   * - initializing SDK,
   * - one-time events.
   */
  info = 'info',

  /**
   * Allowed, but not-optimal, inconsistent state, e.g.:
   * - trying to start monitoring without permissions.
   */
  warning = 'warning',

  /**
   * Not allowed state (any error).
   */
  error = 'error',

  /**
   * No logging at all.
   */
  silent = 'silent',
}

export interface ImportAccountOptions {
  id: string;
  email: string;
  firstName?: string;
  lastName?: string;
  language?: string;
  gender?: string;
  birthDateSeconds?: number;
}

export interface ProfileResponse {
  id: string;
  userName: string;
  claims: string[];
  roles: string[];
  gender: string | null;
  firstName: string;
  lastName: string;
  fullName: string;
  street: string | null;
  houseNumber: string | null;
  zipCode: string | null;
  city: string | null;
  country: string | null;
  landlinePhone: string | null;
  phoneNumber: string | null;
  url: string | null;
  disablePushNotifications: boolean;
  newsLetterSubscription: boolean;
  joinDate: Date;
  birthDate: Date | null;
  clubId: string | null;
  favoriteStore: string | null;
  favoriteStoreId: number | null;
}

/**
 * A class that holds information about the beacon device.
 *
 * It consists of the beacon unique identifier ([uuid], [minor], [major]),
 * the beacon's name ([name]) and the name of the [store] that it is located in.
 *
 * Additionally, it contains data related to its signal strength and the number
 * of times it was contacted.
 */
export interface BeaconContactSummary {
  /**
   * Beacon's uuid.
   */
  uuid: string;

  /**
   * Beacon's major.
   */
  major: string;

  /**
   * Beacon's minor.
   */
  minor: string;

  /**
   * Beacon's name.
   */
  name: string;

  /**
   * The name of the store that the beacon is located in.
   */
  store: string;

  /**
   * Beacon's current signal strength in dB. Null if the beacon has not been contacted yet.
   * RSSI is an abbreviation from Received Signal Strength Indicator.
   * See: https://en.wikipedia.org/wiki/Received_signal_strength_indication
   */
  currentRSSI: number | null;

  /**
   * Beacon's average signal strength in dB (based on the last contacts).
   * Null if the beacon has not been contacted yet.
   */
  averageRSSI: number | null;

  /**
   * Last beacon contact timestamp represented as unix timestamp in milliseconds.
   * Null when the beacon has not been contacted yet.
   */
  lastContactTimestamp: number | null;

  /**
   * Contacts frequency per minute.
   */
  contactFrequencyPerMinute: number;

  /**
   * Indicates how many times the beacon was contacted.
   */
  contactsCount: number;

  /**
   * Returns if the beacon is within range.
   */
  isInRange: boolean;
}

export interface BeaconContactSummaryResponse {
  beacons: BeaconContactSummary[];
}

export interface Offer {
  offerId: number;
  title: string;
  text: string;
  imageUrl: string;
  thumbnailUrl: string;
  validity: string;
  isRedeemed: boolean;
  isRedeemable: boolean;
  isVoucher: boolean;
  tileAmount: number;
  callToAction: { title: string; link: string } | null;
}

export interface OffersResponse {
  offers: Offer[];
}
