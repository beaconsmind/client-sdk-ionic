

## [0.3.0](https://gitlab.com/beaconsmind/client-sdk-ionic/compare/0.2.0...0.3.0) (2023-05-31)


### Features

* add countryCode parameter to the signUp method ([ad63282](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/ad63282e0bf0ff4eb72200a4419743ddf164b301))
* **example:** add country code field to signup screen & display it on profile screen ([01c9d66](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/01c9d661d86b1baf53c84fa8670474c026ee13d8))

## [0.2.0](https://gitlab.com/beaconsmind/client-sdk-ionic/compare/0.1.0...0.2.0) (2023-05-24)


### Features

* upgrade android sdk to 1.12.2 ([3a93194](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/3a93194ae75c0020a3ca899fe2a9ea7c7f7ed4e5))
* upgrade ios sdk to 3.8.3 ([fcff76e](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/fcff76e895156bb686dc4abb44663352b26f9f5e))


### Bug Fixes

* add BeaconsmindSdkInitializer to the package manifest ([2cfa900](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/2cfa900154c08d44b599b774d4b8131001c99341))
* change 'beaconsmind Ionic SDK Demo' to 'Ionic SDK Demo' for consistency ([b49ce6a](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/b49ce6ab96b8771c9c3917fcaf3a3e6339959372))
* **example:** add BGTaskSchedulerPermittedIdentifiers to Info.plist ([38d3afb](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/38d3afbacc7a0e0fc9904961e7becd6b44290a6a))
* **example:** fix product name ([f953007](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/f953007b54dc493af51b3455853e2b6129699295))
* **example:** remove archivesBaseName from build.gradle so that 'ionic capacitor run android' can find the apk file ([b526c79](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/b526c79d8e935b937c2c480da2a095260db5b4b9))
* **example:** remove hardcoded login and password from LoginPage ([d87716d](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/d87716deff6fbffb4408d08daf3404d5c1f13be2))
* **example:** rename iOS example app from 'App' to 'beaconsmind Ionic SDK Demo' ([29840b2](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/29840b2a689b22e4c6dfadaf61358086d003bcad))
* make npm run verify green ([fad5715](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/fad571594b9fa83dfeeb181c7b898fad0155e3da))

## 0.1.0 (2022-11-09)


### Features

* add simple example app ([262ab1f](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/262ab1f0d94369d34295cfd86f0bb30655c0d529))
* added the initializeDevelop  method that will automatically request required permissions and set the log level to minimum. ([4a16719](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/4a1671913bef59034e07252c73ce28455b048ca6))
* show loader when logging out ([6f6b8df](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/6f6b8dfebddac181ca871cf957596d61c4e81e50))


### Bug Fixes

* added `isRedeemable` field to the the `Offer` model structure and renamed `buttonModel` to `callToAction` ([eb7a8e0](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/eb7a8e011d9f03ce6fa22e40036ebb0b1cc6d598))
* fixed the iOS implementation of markOfferAsRedeemed by calling markOfferAsRedeemed instead of markOfferAsReceived ([81b7f60](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/81b7f60db16ae07f1aeecd68d1ca8c5664c8e192))
* offer deserialisation on Android ([991539b](https://gitlab.com/beaconsmind/client-sdk-ionic/commit/991539bc7f2bba248c309217486e303dd7cba84e))