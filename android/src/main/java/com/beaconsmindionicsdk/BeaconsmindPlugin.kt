package com.beaconsmindionicsdk

import com.beaconsmind.api.models.*
import com.beaconsmind.sdk.*
import com.beaconsmind.sdk.models.LogLevel
import com.getcapacitor.*
import com.getcapacitor.annotation.CapacitorPlugin
import java.sql.Date
import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat


@CapacitorPlugin(name = "Beaconsmind")
class BeaconsmindPlugin : Plugin() {
    @PluginMethod
    fun initialize(call: PluginCall) {
        activity.runOnUiThread {
            try {
                if (!call.data.has("hostname")) {
                    throw Exception("Hostname is required.")
                }

                val hostname = call.data.getString("hostname")!!
                val appVersion = call.data.getString("appVersion") ?: "unknown"

                val platformOptions = call.data.optJSONObject("platformOptions")
                val androidOptions = platformOptions?.optJSONObject("android")

                val usePassiveScanning = androidOptions?.optBoolean("usePassiveScanning") ?: true

                val notificationOptions = androidOptions?.optJSONObject("notification")
                val notificationBadgeName =
                    notificationOptions?.optString("androidNotificationBadgeName") ?: "ic_beacons"
                val notificationBadge = context.resources.getIdentifier(
                    notificationBadgeName,
                    "drawable",
                    context.packageName
                )
                val notificationTitle =
                    notificationOptions?.optString("androidNotificationTitle") ?: "Beaconsmind"
                val notificationText = notificationOptions?.optString("androidNotificationText")
                    ?: "Listening for Beacons"
                val notificationChannelName =
                    notificationOptions?.optString("androidNotificationChannelName")
                        ?: "beaconsmind"

                val config = BeaconsmindConfig.Builder(hostname)
                    .setAppVersion(appVersion)
                    .usePassiveScanning(usePassiveScanning)
                    .setNotificationBadge(notificationBadge)
                    .setNotificationTitle(notificationTitle)
                    .setNotificationText(notificationText)
                    .setNotificationChannelName(notificationChannelName)
                    .build()

                BeaconsmindSdk.initialize(activity.application, config)

                val result = JSObject()
                result.put("result", true)
                call.resolve(result)
            } catch (e: Exception) {
                call.reject("initialize failed", e)
            }
        }
    }

    @PluginMethod
    fun initializeDevelop(call: PluginCall) {
        activity.runOnUiThread {
            try {
                if (!call.data.has("hostname")) {
                    throw Exception("Hostname is required.")
                }

                val hostname = call.data.getString("hostname")!!
                val appVersion = call.data.getString("appVersion") ?: "unknown"

                val platformOptions = call.data.optJSONObject("platformOptions")
                val androidOptions = platformOptions?.optJSONObject("android")

                val usePassiveScanning = androidOptions?.optBoolean("usePassiveScanning") ?: true

                val notificationOptions = androidOptions?.optJSONObject("notification")
                val notificationBadgeName =
                    notificationOptions?.optString("androidNotificationBadgeName") ?: "ic_beacons"
                val notificationBadge = context.resources.getIdentifier(
                    notificationBadgeName,
                    "drawable",
                    context.packageName
                )
                val notificationTitle =
                    notificationOptions?.optString("androidNotificationTitle") ?: "Beaconsmind"
                val notificationText = notificationOptions?.optString("androidNotificationText")
                    ?: "Listening for Beacons"
                val notificationChannelName =
                    notificationOptions?.optString("androidNotificationChannelName")
                        ?: "beaconsmind"

                val config = BeaconsmindConfig.Builder(hostname)
                    .setAppVersion(appVersion)
                    .usePassiveScanning(usePassiveScanning)
                    .setNotificationBadge(notificationBadge)
                    .setNotificationTitle(notificationTitle)
                    .setNotificationText(notificationText)
                    .setNotificationChannelName(notificationChannelName)
                    .build()



                BeaconsmindSdk.initializeDevelop(activity, config)

                val result = JSObject()
                result.put("result", true)
                call.resolve(result)
            } catch (e: Exception) {
                call.reject("initializeDevelop failed", e)
            }
        }
    }

    @PluginMethod
    fun signUp(call: PluginCall) {
        try {
            val username = call.getString("username")!!
            val firstName = call.getString("firstName")!!
            val lastName = call.getString("lastName")!!
            val password = call.getString("password")!!
            val confirmPassword = call.getString("confirmPassword")!!
            val language = call.getString("language", null)
            val gender = call.getString("gender", null)
            val favoriteStoreID = call.getInt("favoriteStoreID", null)
            val birthDateSeconds = call.getDouble("birthDateSeconds", null)

            var birthday: Date? = null
            if (birthDateSeconds != null) {
                val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
                birthday = Date(stamp.time)
            }

            val countryCode = call.getString("countryCode", null)

            val request = CreateUserRequest()
            request.username = username
            request.firstName = firstName
            request.lastName = lastName
            request.password = password
            request.confirmPassword = confirmPassword
            request.language = language
            request.gender = gender
            request.favoriteStoreId = favoriteStoreID
            request.birthday = birthday
            request.countryCode = countryCode

            UserManager.getInstance().createAccount(request, object : OnCompleteListener {
                override fun onSuccess() {
                    val userId = checkUser()
                    if (userId != null) {
                        val result = JSObject()
                        result.put("userId", userId)
                        call.resolve(result)
                    } else {
                        call.resolve(null)
                    }
                }

                override fun onError(e: Error?) {
                    call.reject("signup failed: $e")
                }
            }
            )
        } catch (e: Exception) {
            call.reject("signup failed: $e")
        }
    }

    @PluginMethod
    fun login(call: PluginCall) {
        try {
            val userName = call.getString("username")!!
            val password = call.getString("password")!!

            // TODO: ingestivate error: kotlin.UninitializedPropertyAccessException: lateinit property INSTANCE has not been initialized
            UserManager.getInstance()
                .login(userName, password, object : OnCompleteListener {
                    override fun onSuccess() {
                        val userId = checkUser()
                        if (userId != null) {
                            val result = JSObject()
                            result.put("userId", userId)
                            call.resolve(result)
                        } else {
                            call.resolve(null)
                        }
                    }

                    override fun onError(error: Error?) {
                        call.reject("login failed: $error")
                    }
                }
                )
        } catch (e: Exception) {
            call.reject("login failed: $e")
        }
    }

    @PluginMethod
    fun importAccount(call: PluginCall) {
        try {
            val id = call.getString("id")!!
            val email = call.getString("email")!!
            val firstName = call.getString("firstName")
            val lastName = call.getString("lastName")
            val language = call.getString("language")
            val gender = call.getString("gender")
            val birthDateSeconds = call.getDouble("birthDateSeconds", null)

            var birthday: Date? = null
            if (birthDateSeconds != null) {
                val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
                birthday = Date(stamp.time)
            }

            val request = ImportUserRequest()
            request.id = id
            request.email = email
            request.firstName = firstName
            request.lastName = lastName
            request.language = language
            request.gender = gender
            request.birthDate = birthday

            UserManager.getInstance().importAccount(request, object : OnCompleteListener {
                override fun onSuccess() {
                    val userId = checkUser()
                    if (userId != null) {
                        val result = JSObject()
                        result.put("userId", userId)
                        call.resolve(result)
                    } else {
                        call.resolve(null)
                    }
                }

                override fun onError(e: Error) {
                    call.reject("importAccount failed: $e")
                }
            }
            )
        } catch (e: Exception) {
            call.reject("importAccount failed: $e")
        }
    }

    @PluginMethod
    fun logout(call: PluginCall) {
        try {
            UserManager.getInstance().logout()
            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
            checkUser()
        } catch (e: Exception) {
            call.reject("logout failed: $e")
        }
    }

    @PluginMethod
    fun getProfile(call: PluginCall) {
        try {
            UserManager
                .getInstance()
                .getAccount(object : OnResultListener<ProfileResponse> {
                    override fun onError(e: Error) {
                        call.reject("getProfile failed $e")
                    }

                    override fun onSuccess(
                        profileResponse: ProfileResponse?
                    ) {
                        call.resolve(profileResponse?.toWritableMap())
                    }
                })
        } catch (e: Exception) {
            call.reject("getProfile failed: $e")
        }
    }

    @PluginMethod
    fun updateProfile(call: PluginCall) {
        try {
            val firstName = call.getString("firstName")!!
            val lastName = call.getString("lastName")!!
            val city = call.getString("city")
            val country = call.getString("country")
            val disablePushNotifications = call.getBoolean("disablePushNotifications", null)
            val favoriteStoreID = call.getInt("favoriteStoreID", null)
            val gender = call.getString("gender")
            val houseNumber = call.getString("houseNumber")
            val landlinePhone = call.getString("landlinePhone")
            val language = call.getString("language")
            val phoneNumber = call.getString("phoneNumber")
            val street = call.getString("street")
            val zipCode = call.getString("zipCode")
            val birthDateSeconds = call.getDouble("birthDateSeconds", null)

            var birthday: Date? = null
            if (birthDateSeconds != null) {
                val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
                birthday = Date(stamp.time)
            }

            val request = UpdateProfileRequest()
            request.firstName = firstName
            request.lastName = lastName
            request.birthDate = birthday
            request.city = city
            request.country = country
            request.disablePushNotifications = disablePushNotifications?.toString()
            request.favoriteStoreId = favoriteStoreID
            request.gender = gender
            request.houseNumber = houseNumber
            request.landlinePhone = landlinePhone
            request.language = language
            request.phoneNumber = phoneNumber
            request.street = street
            request.zipCode = zipCode

            UserManager.getInstance().updateAccount(request, object : OnCompleteListener {
                override fun onSuccess() {
                    checkUser()
                    UserManager.getInstance()
                        .getAccount(object : OnResultListener<ProfileResponse> {
                            override fun onError(e: Error) {
                                call.reject("updateProfile failed: $e")
                            }

                            override fun onSuccess(profileResponse: ProfileResponse?) {
                                call.resolve(profileResponse?.toWritableMap())
                            }
                        })
                }

                override fun onError(e: Error) {
                    call.reject("updateProfile failed: $e")
                }
            })
        } catch (e: Exception) {
            call.reject("updateProfile failed: $e")
        }
    }

    @PluginMethod
    fun getOAuthContext(call: PluginCall) {
        try {
            val userId = checkUser()
            if (userId != null) {
                var map = JSObject()
                map.put("userId", userId)
                call.resolve(map)
            } else {
                call.resolve(null)
            }
        } catch (e: Exception) {
            call.reject("getOAuthContext failed: $e")
        }
    }

    @PluginMethod
    fun updateHostname(call: PluginCall) {
        try {
            if (!call.data.has("hostname")) {
                throw Exception("Hostname is required.")
            }

            val hostname = call.getString("hostname")!!
            BeaconsmindSdk.updateSuiteUri(hostname)

            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("updateHostname failed: $e")
        }
    }

    @PluginMethod
    fun startListeningBeacons(call: PluginCall) {
        try {
            BeaconListenerService.getInstance().startListeningBeacons()
            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("startListeningBeacons failed: $e")
        }
    }

    @PluginMethod
    fun stopListeningBeacons(call: PluginCall) {
        try {
            BeaconListenerService.getInstance().stopListeningBeacons()
            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("stopListeningBeacons failed: $e")
        }
    }

    @PluginMethod
    fun getBeaconContactsSummary(call: PluginCall) {
        try {
            val beacons = BeaconListenerService.getInstance().beaconsSummary

            val array = JSArray()
            beacons.forEach {
                val entry = JSObject()

                entry.put("uuid", it.uuid)
                entry.put("major", it.major)
                entry.put("minor", it.minor)
                entry.put("name", it.name)
                entry.put("store", it.store)
                entry.put("currentRSSI", it.currentRssi)
                entry.put("averageRSSI", it.averageRssi)
                entry.put("lastContactTimestamp", it.getLastContact()?.toDouble())
                entry.put("contactFrequencyPerMinute", it.getFrequency())
                entry.put("contactsCount", it.timesContacted)
                entry.put("isInRange", it.isInRange)

                array.put(entry)
            }

            val ret = JSObject()
            ret.put("beacons", array)
            call.resolve(ret)
        } catch (e: Exception) {
            call.reject("getBeaconContactsSummary failed: $e")
        }
    }

    @PluginMethod
    fun registerDeviceToken(call: PluginCall) {
        try {
            val token = call.getString("deviceToken")!!
            UserManager.getInstance().updateDeviceInfo(token)
            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("registerDeviceToken failed: $e")
        }
    }

    @PluginMethod
    fun markOfferAsRead(call: PluginCall) {
        try {
            val offerId = call.getInt("offerId")!!
            OffersManager.getInstance().markOfferAsRead(offerId)
            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("markOfferAsRead failed: $e")
        }
    }

    @PluginMethod
    fun markOfferAsReceived(call: PluginCall) {
        try {
            val offerId = call.getInt("offerId")!!
            OffersManager.getInstance().markOfferAsReceived(offerId)
            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("markOfferAsReceived failed: $e")
        }
    }

    @PluginMethod
    fun markOfferAsRedeemed(call: PluginCall) {
        try {
            val offerId = call.getInt("offerId")!!
            OffersManager.getInstance()
                .markOfferRedeemed(offerId, object : OnCompleteListener {
                    override fun onSuccess() {
                        val result = JSObject()
                        result.put("result", true)
                        call.resolve(result)
                    }

                    override fun onError(error: Error) {
                        call.reject("markOfferAsRedeemed failed: $error")
                    }
                })
        } catch (e: Exception) {
            call.reject("markOfferAsRedeemed failed: $e")
        }
    }

    @PluginMethod
    fun loadOffers(call: PluginCall) {
        try {
            OffersManager.getInstance()
                .loadOffers(object : OnResultListener<List<OfferResponse>> {
                    override fun onError(error: Error) {
                        call.reject("loadOffers failed: $error")
                    }

                    override fun onSuccess(response: List<OfferResponse>) {
                        val array = JSArray()
                        response.forEach { offer -> array.put(offer.toJSObject()) }

                        val result = JSObject()
                        result.put("offers", array)
                        call.resolve(result)
                    }
                })
        } catch (e: Exception) {
            call.reject("loadOffers failed: $e")
        }
    }

    @PluginMethod
    fun loadOffer(call: PluginCall) {
        try {
            val offerId = call.getInt("offerId")!!
            OffersManager.getInstance()
                .loadOffer(offerId, object : OnResultListener<OfferResponse> {
                    override fun onError(error: Error) {
                        call.reject("loadOffer failed: $error")
                    }

                    override fun onSuccess(
                        response: OfferResponse
                    ) {
                        call.resolve(response.toJSObject())
                    }
                })
        } catch (e: Exception) {
            call.reject("loadOffer failed: $e")
        }
    }

    @PluginMethod
    fun setMinLogLevel(call: PluginCall) {
        try {
            val minLogLevelString = call.getString("minLogLevel", "")!!
            val minLogLevel = LogLevel.fromValue(minLogLevelString)

            BeaconsmindSdk.minLogLevel = minLogLevel

            val result = JSObject()
            result.put("result", true)
            call.resolve(result)
        } catch (e: Exception) {
            call.reject("setMinLogLevel failed: $e")
        }
    }

    private fun checkUser(): String? {
        val userManager = UserManager.getInstance()
        return userManager.accessToken
    }
}

fun OfferResponse.toJSObject(): JSObject {
    val entry = JSObject()
    entry.put("offerId", offerId)
    entry.put("title", title)
    entry.put("text", text)
    entry.put("imageUrl", imageUrl)
    entry.put("thumbnailUrl", thumbnailUrl)
    entry.put("validity", validity)
    entry.put("isRedeemed", isRedeemed)
    entry.put("isRedeemable", isRedeemable)

    callToAction?.let {
        val callToActionEntry = JSObject()
        callToActionEntry.put("title", it.title)
        callToActionEntry.put("link", it.link)
        entry.put("callToAction", callToActionEntry)
    }

    return entry
}

fun ProfileResponse.toWritableMap(): JSObject {
    val dateFormatter: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ")

    val map = JSObject()

    map.put("disablePushNotifications", disablePushNotifications)
    map.put("firstName", firstName)
    map.put("joinDate", dateFormatter.format(joinDate))
    map.put("lastName", lastName)
    map.put("newsLetterSubscription", newsLetterSubscription)
    map.put("birthDate", birthDate?.let { dateFormatter.format(it) })
    map.put("zipCode", zipCode)
    map.put("street", street)
    map.put("city", city)

    val claimsArray = JSArray()
    for (claim in claims!!) {
        claimsArray.put(claim)
    }
    map.put("claims", claimsArray)

    map.put("clubId", clubId)
    map.put("country", country)
    map.put("favoriteStore", favoriteStore)
    favoriteStoreId?.let { map.put("favoriteStoreId", favoriteStoreId!!) }
    map.put("fullName", fullName)
    map.put("gender", gender)
    map.put("houseNumber", houseNumber)
    map.put("id", id)
    map.put("landlinePhone", landlinePhone)
    map.put("language", language)
    map.put("phoneNumber", phoneNumber)

    val rolesArray = JSArray()
    for (role in roles!!) {
        rolesArray.put(role)
    }
    map.put("roles", rolesArray)

    map.put("street", street)
    map.put("url", url)
    map.put("userName", userName)
    map.put("zipCode", zipCode)

    return map
}