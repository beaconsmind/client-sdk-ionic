import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonLoading,
} from '@ionic/react';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import { RouteComponentProps } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Beaconsmind, Offer } from '@beaconsmind/ionic-sdk';

interface OfferPageProps
  extends RouteComponentProps<{
    id: string;
  }> {}

const OfferPage: React.FC<OfferPageProps> = ({ match }) => {
  const offerId = Number.parseInt(match.params.id);
  const [offer, setOffer] = useState<Offer | null>(null);

  const [presentLoader, dismissLoader] = useIonLoading();

  useEffect(() => {
    Beaconsmind.markOfferAsRead({ offerId: offerId });

    Beaconsmind.loadOffer({ offerId: offerId }).then((offer) => {
      console.log(offer);
      console.log(JSON.stringify(offer));
      setOffer(offer);
    });
  }, [offerId]);

  const showRedeemButton = (offer?.isRedeemable ?? false) && !(offer?.isRedeemed ?? false);

  const showCTAButton = offer?.callToAction !== undefined;

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>{offer?.title ?? ''}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <img
          alt=""
          src={offer?.imageUrl}
          style={{
            width: '100%',
          }}
        />
        <IonCard key={offer?.offerId}>
          <IonCardHeader>
            <IonCardTitle>{offer?.title}</IonCardTitle>
            <IonCardSubtitle>{offer?.validity}</IonCardSubtitle>
          </IonCardHeader>

          <IonCardContent>
            <div dangerouslySetInnerHTML={{ __html: offer?.text ?? '' }} />{' '}
          </IonCardContent>
        </IonCard>
        {showCTAButton && (
          <IonButton
            style={{
              marginLeft: '8px',
              marginRight: '8px',
            }}
            expand="full"
            onClick={() => {
              window.open(offer?.callToAction?.link);
            }}
          >
            {offer?.callToAction?.title}
          </IonButton>
        )}
        {showRedeemButton && (
          <IonButton
            style={{
              marginLeft: '8px',
              marginRight: '8px',
            }}
            expand="full"
            onClick={async () => {
              try {
                presentLoader({
                  message: 'Redeeming offer...',
                });

                await Beaconsmind.markOfferAsRedeemed({
                  offerId: offerId,
                });

                const offer = await Beaconsmind.loadOffer({
                  offerId: offerId,
                });

                setOffer(offer);
              } catch (e) {
                presentAlert({
                  header: 'Redeeming offer failed',
                  message: 'Please try again.',
                });
              }

              dismissLoader();
            }}
          >
            Redeem
          </IonButton>
        )}
      </IonContent>
    </IonPage>
  );
};

export default OfferPage;
function presentAlert(arg0: { header: string; message: string }) {
  throw new Error('Function not implemented.');
}
