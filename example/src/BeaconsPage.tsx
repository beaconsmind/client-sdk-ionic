import React, { useEffect, useState } from 'react';
import {
  IonBackButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonLoading,
} from '@ionic/react';
import { RouteComponentProps } from 'react-router';

import { Beaconsmind, BeaconContactSummary } from '@beaconsmind/ionic-sdk';

const BeaconsPage: React.FC<RouteComponentProps> = ({ history }) => {
  const [presentLoader, dismissLoader] = useIonLoading();
  const [beacons, setBeacons] = useState<BeaconContactSummary[]>([]);

  useEffect(() => {
    presentLoader();
    Beaconsmind.getBeaconContactsSummary().then((response) => {
      setBeacons(response.beacons);
      dismissLoader();
    });

    const interval = setInterval(() => {
      Beaconsmind.getBeaconContactsSummary().then((response) => {
        setBeacons(response.beacons);
      });
    }, 1000);

    return () => {
      clearInterval(interval);
      dismissLoader();
    };
  }, [presentLoader, dismissLoader]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Beacons</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList>
          {beacons.map((beacon) => (
            <IonCard key={beacon.uuid + beacon.minor + beacon.major} color={beacon.isInRange ? 'success' : ''}>
              <IonCardHeader>
                <IonCardTitle>{beacon.name}</IonCardTitle>
                <IonCardSubtitle>{beacon.store}</IonCardSubtitle>
              </IonCardHeader>

              <IonCardContent>
                {beacon.lastContactTimestamp && (
                  <IonLabel>Last contact: {new Date(beacon.lastContactTimestamp).toLocaleString()}</IonLabel>
                )}
              </IonCardContent>
            </IonCard>
          ))}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default BeaconsPage;
