import { IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

import React from 'react';

import { PushNotifications } from '@capacitor/push-notifications';

import { RouteComponentProps } from 'react-router-dom';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import { useEffect } from 'react';

import { Beaconsmind } from '@beaconsmind/ionic-sdk';
import { useAppContext } from './App';

async function setupNotifications(history: any): Promise<() => void> {
  let permStatus = await PushNotifications.checkPermissions();
  if (permStatus.receive === 'prompt') {
    permStatus = await PushNotifications.requestPermissions();
  }

  if (permStatus.receive !== 'granted') {
    console.log('User denied permissions!');
  }

  try {
    await PushNotifications.createChannel({
      id: 'beaconsmind',
      name: 'Beaconsmind sdk demo',
    });
  } catch {}

  await PushNotifications.register();

  const registrationHandle = PushNotifications.addListener('registration', async (token) => {
    console.log('Registration token: ' + token.value);

    const registerDeviceTokenResponse = await Beaconsmind.registerDeviceToken({
      deviceToken: token.value,
    });

    console.log(JSON.stringify(registerDeviceTokenResponse));
  });

  const registrationErrorHandle = PushNotifications.addListener('registrationError', (err) => {
    console.log('Registration error:');
    console.log(JSON.stringify(err));
  });

  const pushNotificationReceivedHandle = PushNotifications.addListener('pushNotificationReceived', (notification) => {
    console.log('pushNotificationReceived');
    console.log(JSON.stringify(notification));

    const id = parseInt(notification.data.offerId);
    Beaconsmind.markOfferAsReceived({ offerId: id });

    history.push(`/offers/${id}`);
  });

  const pushNotificationActionPerformedHandle = PushNotifications.addListener(
    'pushNotificationActionPerformed',
    (notification) => {
      console.log('pushNotificationActionPerformed');
      console.log(JSON.stringify(notification));

      const id = notification.notification.data.offerId;

      history.push(`/offers/${id}`);
    }
  );

  return () => {
    registrationHandle.remove();
    registrationErrorHandle.remove();
    pushNotificationReceivedHandle.remove();
    pushNotificationActionPerformedHandle.remove();
  };
}

async function setupBeaconsmind(hostname: string) {
  const initializeResponse = await Beaconsmind.initializeDevelop({
    hostname: hostname,
    appVersion: '0.0.1',
    platformOptions: {
      android: {
        usePassiveScanning: true,
        notification: {
          androidNotificationBadgeName: 'ic_beacons',
          androidNotificationChannelName: 'beaconsmind',
          androidNotificationTitle: 'Beaconsmind sdk demo',
          androidNotificationText: 'Listening to beacons',
        },
      },
    },
  });
  console.log(initializeResponse);

  if (initializeResponse.result === false) {
    return;
  }

  const startListeningBeaconsResponse = await Beaconsmind.startListeningBeacons();
  console.log(startListeningBeaconsResponse);
}

const HomePage: React.FC<RouteComponentProps> = ({ history }) => {
  const { isLoggedIn, hostname } = useAppContext();

  useEffect(() => {
    if (!isLoggedIn) {
      console.log('Signing out...');
      history.replace('/login');
    }
  }, [history, isLoggedIn]);

  useEffect(() => {
    const disposeNotifications = setupNotifications(history);

    setupBeaconsmind(hostname);

    return () => {
      disposeNotifications.then((dispose) => dispose());
    };
  }, [history]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Beaconsmind</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          onClick={() => {
            history.push('/beacons');
          }}
          expand="full"
        >
          Beacons
        </IonButton>
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          onClick={() => {
            history.push('/offers');
          }}
          expand="full"
        >
          Offers
        </IonButton>
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          onClick={() => {
            history.push('/profile');
          }}
          expand="full"
        >
          Profile
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default HomePage;
