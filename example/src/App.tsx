import { IonApp, IonContent, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import { useStatePersist } from 'use-state-persist';

/* Theme variables */
import LoginPage from './LoginPage';
import React, { useEffect, useState } from 'react';
import HomePage from './HomePage';

import { Route } from 'react-router-dom';
import OfferPage from './OfferPage';
import SignUpPage from './SignUpPage';
import BeaconsPage from './BeaconsPage';
import OffersPage from './OffersPage';
import ProfilePage from './ProfilePage';

import { Beaconsmind } from '@beaconsmind/ionic-sdk';

setupIonicReact();

interface AppState {
  isLoggedIn: boolean;
  setIsLoggedIn: (isLoggedIn: boolean) => void;
  hostname: string;
  setHostname: (hostname: string) => void;
}

export const AppContext = React.createContext<AppState>({
  isLoggedIn: false,
  setIsLoggedIn: () => {},
  hostname: 'https://test-develop-suite.azurewebsites.net/',
  setHostname: () => {},
});

export const useAppContext = () => {
  return React.useContext<AppState>(AppContext);
};

export const AppContextProvider: React.FC = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = useStatePersist<boolean>('isLoggedIn', false);
  const [hostname, setHostname] = useStatePersist<string>(
    'setHostname',
    'https://test-develop-suite.azurewebsites.net/'
  );

  return (
    <AppContext.Provider
      value={{
        isLoggedIn: isLoggedIn,
        setIsLoggedIn: setIsLoggedIn,
        hostname: hostname,
        setHostname: setHostname,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

const App: React.FC = () => {
  return (
    <AppContextProvider>
      <AppRouter />
    </AppContextProvider>
  );
};

const AppRouter: React.FC = () => {
  const { hostname } = useAppContext();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    Beaconsmind.initializeDevelop({
      hostname: hostname,
      appVersion: '0.0.1',
      platformOptions: {
        android: {
          usePassiveScanning: true,
          notification: {
            androidNotificationBadgeName: 'ic_beacons',
            androidNotificationChannelName: 'beaconsmind',
            androidNotificationTitle: 'Beaconsmind sdk demo',
            androidNotificationText: 'Listening to beacons',
          },
        },
      },
    }).then((response) => {
      console.log(response);
      setIsLoading(false);
    });

    return () => {};
  }, [hostname]);

  return (
    <IonApp>
      {isLoading ? (
        <IonContent />
      ) : (
        <IonReactRouter>
          <IonRouterOutlet animated={false}>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/beacons" component={BeaconsPage} />
            <Route exact path="/offers" component={OffersPage} />
            <Route exact path="/profile" component={ProfilePage} />
            <Route exact path="/offers/:id" component={OfferPage} />
            <Route exact path="/signup" component={SignUpPage} />
            <Route exact path="/login" component={LoginPage} />
          </IonRouterOutlet>
        </IonReactRouter>
      )}
    </IonApp>
  );
};

export default App;
