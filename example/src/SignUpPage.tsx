import { IonButton, IonContent, IonHeader, IonInput, IonPage, IonTitle, IonToolbar } from '@ionic/react';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
import { useIonAlert } from '@ionic/react';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import { useState } from 'react';

import { Beaconsmind } from '@beaconsmind/ionic-sdk';
import { useAppContext } from './App';
import { RouteComponentProps } from 'react-router-dom';
import { useIonLoading } from '@ionic/react';

const SignUpPage: React.FC<RouteComponentProps> = ({ history }) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [countryCode, setCountryCode] = useState('');

  const { setIsLoggedIn } = useAppContext();
  const [presentAlert] = useIonAlert();
  const [presentLoader, dismissLoader] = useIonLoading();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>SignUp</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={firstName}
          type="text"
          title="First name"
          autoCapitalize="words"
          placeholder="First name"
          onIonChange={(e) => {
            setFirstName(e.detail.value!);
          }}
        />
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={lastName}
          type="text"
          title="Last name"
          autoCapitalize="words"
          placeholder="Last name"
          onIonChange={(e) => {
            setLastName(e.detail.value!);
          }}
        />
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={email}
          type="email"
          title="Email"
          placeholder="Email"
          onIonChange={(e) => {
            setEmail(e.detail.value!);
          }}
        />
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={password}
          type="password"
          title="Password"
          placeholder="Password"
          onIonChange={(e) => {
            setPassword(e.detail.value!);
          }}
        />
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={confirmPassword}
          type="password"
          title="Confirm password"
          placeholder="Confirm password"
          onIonChange={(e) => {
            setConfirmPassword(e.detail.value!);
          }}
        />

        {/**
         * TODO(korzonkiee): It should be converted to a dropdown.
         * The list of country codes can be fetched from the SDK
         * https://beaconsmind.monday.com/boards/2736418117/pulses/4549538459
         **/}
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={countryCode}
          type="text"
          title="Country code"
          placeholder="Country (3 letter code, e.g. CHE, DEU, POL, SAU)"
          onIonChange={(e) => {
            setCountryCode(e.detail.value!);
          }}
        />
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          expand="full"
          onClick={async () => {
            try {
              presentLoader({
                message: 'Signing up...',
              });

              const result = await Beaconsmind.signUp({
                username: email,
                firstName: firstName,
                lastName: lastName,
                password: password,
                confirmPassword: confirmPassword,
                countryCode: countryCode,
              });

              console.log(result);
              setIsLoggedIn(true);
              history.replace('/');
            } catch (e) {
              presentAlert({
                header: 'Sign up failed',
                message: 'The account you try to create may already exist.',
              });
            }

            dismissLoader();
          }}
        >
          Create account
        </IonButton>
        <IonButton
          style={{
            marginTop: '16px',
          }}
          fill="clear"
          expand="full"
          onClick={async () => {
            history.replace('/login');
          }}
        >
          Already have an account? Tap here to login.
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default SignUpPage;
