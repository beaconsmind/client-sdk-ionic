import React, { useEffect, useState } from 'react';
import {
  IonBackButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonLoading,
} from '@ionic/react';

import { Beaconsmind, Offer } from '@beaconsmind/ionic-sdk';
import { RouteComponentProps } from 'react-router-dom';

const OffersPage: React.FC<RouteComponentProps> = ({ history }) => {
  const [presentLoader, dismissLoader] = useIonLoading();
  const [offers, setOffers] = useState<Offer[]>([]);

  useEffect(() => {
    presentLoader();
    Beaconsmind.loadOffers().then((response) => {
      setOffers(response.offers);
      dismissLoader();
    });
  }, [presentLoader, dismissLoader]);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Offers</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList>
          {offers.map((offer) => (
            <IonCard
              key={offer.offerId}
              onClick={() => {
                history.push('/offers/' + offer.offerId);
              }}
            >
              <IonCardHeader>
                <IonCardTitle>{offer.title}</IonCardTitle>
                <IonCardSubtitle>{offer.validity}</IonCardSubtitle>
              </IonCardHeader>

              <IonCardContent>
                <div dangerouslySetInnerHTML={{ __html: offer?.text ?? '' }} />{' '}
              </IonCardContent>
            </IonCard>
          ))}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default OffersPage;
