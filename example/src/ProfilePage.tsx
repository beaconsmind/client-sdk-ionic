import React, { useEffect, useState } from 'react';
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonItemDivider,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonAlert,
  useIonLoading,
} from '@ionic/react';

import { ProfileResponse, Beaconsmind } from '@beaconsmind/ionic-sdk';
import { RouteComponentProps } from 'react-router-dom';
import { useAppContext } from './App';

const ProfilePage: React.FC<RouteComponentProps> = ({ history }) => {
  const { setIsLoggedIn } = useAppContext();

  const [profile, setProfile] = useState<ProfileResponse | null>(null);

  const [presentLoader, dismissLoader] = useIonLoading();

  useEffect(() => {
    Beaconsmind.getProfile().then((profile) => {
      console.log(JSON.stringify(profile));
      setProfile(profile);
    });
  }, []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Profile</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonItem>
          <IonLabel
            style={{
              paddingTop: '16px',
              marginLeft: '8px',
              marginRight: '8px',
            }}
          >
            {profile?.userName}
          </IonLabel>
        </IonItem>
        <IonItem>
          <IonLabel
            style={{
              paddingTop: '16px',
              marginLeft: '8px',
              marginRight: '8px',
            }}
          >
            {profile?.firstName}
          </IonLabel>
        </IonItem>
        <IonItem>
          <IonLabel
            style={{
              paddingTop: '16px',
              marginLeft: '8px',
              marginRight: '8px',
            }}
          >
            {profile?.lastName}
          </IonLabel>
        </IonItem>
        <IonItem>
          <IonLabel
            style={{
              paddingTop: '16px',
              marginLeft: '8px',
              marginRight: '8px',
            }}
          >
            {profile?.country}
          </IonLabel>
        </IonItem>
        <IonItemDivider />
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          expand="full"
          onClick={async () => {
            try {
              presentLoader({
                message: 'Loggout out...',
              });

              history.goBack();
              await Beaconsmind.logout();
            } finally {
              dismissLoader();
              setIsLoggedIn(false);
            }
          }}
        >
          Logout
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default ProfilePage;
