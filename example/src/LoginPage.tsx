import {
  IonButton,
  IonContent,
  IonHeader,
  IonInput,
  IonItemDivider,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonAlert,
  useIonLoading,
} from '@ionic/react';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import { useState } from 'react';

import { Beaconsmind } from '@beaconsmind/ionic-sdk';
import { useAppContext } from './App';
import { RouteComponentProps } from 'react-router-dom';

const LoginPage: React.FC<RouteComponentProps> = ({ history }) => {
  const { setIsLoggedIn, hostname, setHostname } = useAppContext();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [presentAlert] = useIonAlert();
  const [presentLoader, dismissLoader] = useIonLoading();

  const [url, setUrl] = useState(hostname);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={email}
          type="email"
          title="Email"
          placeholder="Email"
          onIonChange={(e) => {
            setEmail(e.detail.value!);
          }}
        />
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={password}
          type="password"
          title="Password"
          placeholder="Password"
          onIonChange={(e) => {
            setPassword(e.detail.value!);
          }}
        />
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          expand="full"
          fill="solid"
          onClick={async () => {
            try {
              presentLoader({
                message: 'Logging in...',
              });

              const result = await Beaconsmind.login({
                username: email,
                password: password,
              });

              console.log(result);
              setIsLoggedIn(true);
              history.replace('/');
            } catch (e) {
              presentAlert({
                header: 'Login failed',
                message: 'Make sure that the account you try to login with exists.',
              });
            }

            dismissLoader();
          }}
        >
          Login
        </IonButton>
        <IonButton
          style={{
            marginTop: '16px',
          }}
          fill="clear"
          expand="full"
          onClick={async () => {
            history.replace('/signup');
          }}
        >
          Don't have an account? Tap here to sign up.
        </IonButton>
        <IonItemDivider />
        <IonInput
          style={{
            marginLeft: '8px',
            marginRight: '8px',
          }}
          value={url}
          title="Hostname"
          placeholder="Hostname"
          onIonChange={(e) => {
            setUrl(e.detail.value!);
          }}
        />
        <IonButton
          style={{
            marginTop: '16px',
            marginLeft: '8px',
            marginRight: '8px',
          }}
          expand="full"
          fill="solid"
          onClick={async () => {
            try {
              presentLoader({
                message: 'Updating hostname...',
              });

              const response = await Beaconsmind.updateHostname({
                hostname: url,
              });

              console.log(response);

              if (response.result === true) {
                setHostname(url);
              }
            } catch (e) {
              presentAlert({
                header: 'Updating hostname failed',
                message: 'Make sure that the URL is correct.',
              });
            }

            dismissLoader();
          }}
        >
          Update
        </IonButton>
      </IonContent>
    </IonPage>
  );
};

export default LoginPage;
