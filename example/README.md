
# Beaconsmind Ionic SDK example app

This is an example Ionic app that showcases the Beaconsmind SDK.

## Demo

- Requesting permission on app startup ([video](https://share.cleanshot.com/AdSHbe)).
- Signing in ([video](https://share.cleanshot.com/19rxyu)).
- Signing out ([video](https://share.cleanshot.com/f7a1CD)).
- Signing up ([video](https://share.cleanshot.com/Mjfh9I)).
- Browsing beacons ([video](https://share.cleanshot.com/MDPIMd)).
- Browsing offers ([video](https://share.cleanshot.com/dESXUp)).
- Entering offer details ([video](https://share.cleanshot.com/v6ODcO)).
- Tapping on the offer's CTA button ([video](https://share.cleanshot.com/nmiIAU)).
- Navigating to offer screen from push notification ([video](https://share.cleanshot.com/CymH9H)).
- Redeeming the offer ([video](https://share.cleanshot.com/tielUa)).
- Viewing profile ([video](https://share.cleanshot.com/JW9puK)).
- Updating Suite hostname ([video](https://share.cleanshot.com/4tWaNO)).


## Run Locally

Go to the example project directory

```bash
  cd example
```

Install dependencies

```bash
  npm install
```

In order to run the app on your physical device, follow the guide below. Make sure that you have [Ionic CLI](https://ionicframework.com/docs/cli) installed.

### Android

1. Navigate to the `example` directory.
2. Run `ionic capacitor run android -l --external`.

**Debugging**
1. In order to debug native code, use Android Studio.
2. In order to debug JavaScript code, see [this](https://ionicframework.com/docs/developing/android#using-chrome-devtools) guide.

### iOS

1. Navigate to the `example` directory.
2. Run `ionic capacitor run ios -l --external --open`. It will start the development server and open Xcode.
3. In Xcode, select the device and hit the Run button.

**Debugging**
1. In order to debug native code, use Xcode.
2. In order to debug JavaScript code, see [this](https://ionicframework.com/docs/developing/ios#using-safari-web-inspector) guide.


## Deployment

### Android

Go to `example` directory and run:

```bash
cd example
ionic capacitor build android
```

to copy web assets to the native Android platform.


Then, open [variables.gradle](example/android/variables.gradle) file and update `versionName` and `versionCode`. If the example app is referencing Ionic SDK in version `0.1.0`, then set the versionName to `0.1.0`.

```gradle
ext {
    versionCode = 1
    versionName = '0.1.0'
    ...
}
```

Go to `android` directory and run:

```bash
cd android
./gradlew assembleRelease       
```

If the assemble step was successful, an APK will be generated in `app/build/outputs/apk/release/` directory, for instance:

```bash
./app/build/outputs/apk/release/com.beaconsmind.ioniccapacitordemo-v0.1.0(1)-release-unsigned.apk
```

Attach the APK to the corresponding GitLab release. For instance, if the example app is using Beaconsmind SDK version 0.1.0, then it should be attached to a GitLab release with the same version.

### iOS

Go to `example` directory and run:

```bash
cd example
ionic capacitor build ios
```

to copy web assets to the native iOS platform.

Then, open the Xcode project by running:

```bash
open ios/App/App.xcworkspace
```

Go to "App" target > "Build settings" tab > "Versioning" section and update "Marketing Version" and "Current Project Version". If the example app is referencing Ionic SDK in version `0.1.0`, then set the "Marketing Version" to `0.1.0`.

Next, select "Any iOS Device (arm64)" architecture.

Then, from the top options chooe "Product" > "Archive".
## Authors

- [@korzonkiee](https://www.github.com/korzonkiee)

