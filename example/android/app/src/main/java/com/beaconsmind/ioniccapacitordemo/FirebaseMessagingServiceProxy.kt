package com.beaconsmind.ioniccapacitordemo

import android.content.Intent
import com.capacitorjs.plugins.pushnotifications.MessagingService
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.lang.reflect.Field


class FirebaseMessagingServiceProxy : FirebaseMessagingService() {
    private val messagingServices: ArrayList<FirebaseMessagingService> = object : ArrayList<FirebaseMessagingService>() {
        init {
            add(MessagingService())
            //    other messaging services, e.g. Moengage
        }
    }

    override fun onMessageReceived(message: RemoteMessage) {
        delegate(object : GCAction1<FirebaseMessagingService> {
            override fun run(service: FirebaseMessagingService) {
                injectContext(service)
                service.onMessageReceived(message)
            }
        })
    }

    override fun onNewToken(s: String) {
        delegate(object : GCAction1<FirebaseMessagingService> {
            override fun run(t: FirebaseMessagingService) {
                injectContext(t)
                t.onNewToken(s)
            }
        })
    }

    override fun handleIntent(intent: Intent) {
        delegate(object : GCAction1<FirebaseMessagingService> {
            override fun run(t: FirebaseMessagingService) {
                injectContext(t)
                t.handleIntent(intent)
            }
        })
    }

    private fun delegate(action: GCAction1<FirebaseMessagingService>) {
        for (service in messagingServices) {
            service.let { action.run(it) }
        }
    }

    private fun injectContext(service: FirebaseMessagingService) {
        setField(service, "mBase", this)
    }

    private fun setField(targetObject: Any, fieldName: String, fieldValue: Any) {
        var field: Field?
        field = try {
            targetObject.javaClass.getDeclaredField(fieldName)
        } catch (e: NoSuchFieldException) {
            null
        }
        var superClass: Class<*>? = targetObject.javaClass.superclass
        while (field == null && superClass != null) {
            try {
                field = superClass.getDeclaredField(fieldName)
            } catch (e: NoSuchFieldException) {
                superClass = superClass.superclass
            }
        }
        if (field == null) {
            return
        }
        field.isAccessible = true
        try {
            field[targetObject] = fieldValue
        } catch (ignored: IllegalAccessException) {
        }
    }
}

internal interface GCAction1<T> {
    fun run(t: T)
}
