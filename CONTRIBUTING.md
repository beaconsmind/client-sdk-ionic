# Contributing

This guide provides instructions for contributing to this Capacitor plugin.

## Upgrading native SDKs

Before starting with the upgrade process, make sure that both iOS & Android example apps are in the buildable and working state. See example app [README](example/README.md) to learn how to run the apps.

### iOS

1. Check what is the latest version of the iOS SDK [here](https://gitlab.com/beaconsmind/client-sdk-ios/-/releases).
2. Update [BeaconsmindIonicSdk.podspec](BeaconsmindIonicSdk.podspec) to point to the new version:
   ```diff
   -  s.dependency "Beaconsmind", "1.0.0"
   +  s.dependency "Beaconsmind", "1.0.1"
   ```
3. Install updated dependency:
   ```
   cd example/ios/App
   pod update Beaconsmind
   cd ../../..
   ```
4. Open the example app in Xcode and build the app to see if compiles successfully.
   ```
   open example/ios/App/App.xcworkspace
   ```
5. Check what are the changes from the current version that the Ionic SDK is using and the latest version. If needed, speak with the developer responsible for the native platform. If there are new methods that need to be exposed via Ionic binding, make sure to implement them.
6. Run the iOS example app to make sure that everything works correctly after the upgrade.
7. Make sure to test the new bindings if needed.
8. Make sure to update the [README.md](README.MD) if needed.

### Android

1. Check what is the latest version of the Android SDK [here](https://gitlab.com/beaconsmind/client-sdk-android/-/releases).
2. Update [android/build.gradle](android/build.gradle) to point to the new version:
   ```diff
   -  implementation "com.beaconsmind:client-sdk-android:1.0.0"
   +  implementation "com.beaconsmind:client-sdk-android:1.0.1"
   ```
3. Open the example app in Android Studio.
   ```
   open -a /Applications/Android\ Studio.app example/android 
   ```
4. Run the gradle sync and build the project to see if compiles successfully.
5. Check what are the changes from the current version that the Ionic SDK is using and the latest version. If needed, speak with the developer responsible for the native platform. If there are new methods that need to be exposed via Ionic binding, make sure to implement them.
6. Run the Android example app to make sure that everything works correctly after the upgrade.
7. Make sure to test the new bindings if needed.
8. Make sure to update the [README.md](README.MD) if needed.

## Development workflow

### Local Setup

1. Fork and clone the repo.
1. Install the dependencies.

    ```shell
    npm install
    ```

1. Install SwiftLint if you're on macOS.

    ```shell
    brew install swiftlint
    ```

### Scripts

#### `npm run build`

Build the plugin web assets and generate plugin API documentation using [`@capacitor/docgen`](https://github.com/ionic-team/capacitor-docgen).

It will compile the TypeScript code from `src/` into ESM JavaScript in `dist/esm/`. These files are used in apps with bundlers when your plugin is imported.

Then, Rollup will bundle the code into a single file at `dist/plugin.js`. This file is used in apps without bundlers by including it as a script in `index.html`.

#### `npm run verify`

Build and validate the web and native projects.

This is useful to run in CI to verify that the plugin builds for all platforms.

#### `npm run lint` / `npm run fmt`

Check formatting and code quality, autoformat/autofix if possible.

This template is integrated with ESLint, Prettier, and SwiftLint. Using these tools is completely optional, but the [Capacitor Community](https://github.com/capacitor-community/) strives to have consistent code style and structure for easier cooperation.

### npm run release

We use [release-it](https://github.com/release-it/release-it) to make it easier to publish new versions. It handles common tasks like bumping version based on semver, creating tags and releases etc.

> **Note**: The [`files`](https://docs.npmjs.com/cli/v7/configuring-npm/package-json#files) array in `package.json` specifies which files get published. If you rename files/directories or add files elsewhere, you may need to update it.
